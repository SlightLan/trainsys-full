from django.shortcuts import render, HttpResponseRedirect, redirect


def mLogin_required(func):
    def check_login_status(request,*args,**kwargs):
        if request.session.has_key('is_login'):
            # 当前有用户登录，正常跳转
            return func(request,*args,**kwargs)
        else:
            # 此时需要获取用户所点击的url，并保存在cookie中，再跳转到登录页面。
            response = HttpResponseRedirect('/login/')
            # 用户点击链接，会发送GET请求，对应的request对象中，含有要请求的url地址、请求参数、等等。
            response.set_cookie('prev_url', request.path)
            return response
    return check_login_status
