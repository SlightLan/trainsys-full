IMG_SERVER = "http://114.55.172.31/img/"
IMG_SERVER_UPLOAD = IMG_SERVER + "upload/"
HEADER_IMG = "/static/img/jumbotron.png"
TEST_INFO_HEADER_IMG = "/static/img/classPageHeader.png"


def global_utils(request):
    return {
        "IMG_SERVER": IMG_SERVER,
        "HEADER_IMG": HEADER_IMG,
        "IMG_SERVER_UPLOAD": IMG_SERVER_UPLOAD,
        "TEST_INFO_HEADER_IMG": TEST_INFO_HEADER_IMG,
    }
