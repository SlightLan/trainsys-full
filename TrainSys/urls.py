"""TrainSys URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from login import login_views
from class_op import class_op_views
from person_info import person_info_views
from test_op import test_op_views
from grade import grade_views

urlpatterns = [
    path('', login_views.login),
    path('admin/', admin.site.urls),

    # urls of login app, need to be moved inside login app
    path('login/', login_views.login),
    path('logout/', login_views.logout),
    path('register/', login_views.register),
    path('register/teacher_verify/', login_views.teacher_verify),

    path('classes/', include('class_op.urls')),
    path('user/', person_info_views.index),
    path('tests/', include('test_op.urls')),
    path('grade/', include('grade.urls')),
    path('main_session/', include('main_session.urls')),
    path('api/', include('api.urls')),
]
