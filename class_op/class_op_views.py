from django.shortcuts import render, redirect, reverse, HttpResponse
from django.contrib import messages
from . import models
from login.models import User
from test_op.models import Test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json

from TrainSys.global_func import mLogin_required


# Create your views here.


def index(request):
    return explore_classes(request)


def generate_class_ob(c):
    class_ob = {
        'id': c.id,
        'name': c.name,
        'teachers': c.teachers.all(),
        'students_number': len(c.students.all()),
        'describe': c.describe,
        'cover': (c.cover + '?w=125&h=100'),
        'create_time': c.create_time,
    }
    return class_ob


def paginator_classes(classes, page):
    count = 0
    set4 = []
    all_classes = []
    data = {}
    for c in classes:
        class_ob = {
            'id': c.id,
            'name': c.name,
            'teachers': c.teachers,
            'students_number': len(c.students.all()),
            'cover': (c.cover + '?w=250&h=200'),
        }
        if count < 4:
            set4.append(class_ob)
            count += 1
        else:
            all_classes.append(set4)
            set4 = []
            count = 0
    if count != 0:
        all_classes.append(set4)
    paginator = Paginator(all_classes, 16)
    if page:
        try:
            page = int(page)
            page_data = paginator.page(page)
        except PageNotAnInteger:
            page_data = paginator.page(1)
        except EmptyPage:
            page_data = paginator.page(paginator.num_pages)
        data['page_data'] = page_data
    else:
        page_data = paginator.page(1)
        data['page_data'] = page_data
    return data


@mLogin_required
def joined_classes(request):
    if request.method == "GET":
        page = request.GET.get('page')
        user_id = request.session.get('user_id')
        if user_id:
            user = User.objects.get(id=user_id)
            data = paginator_classes(user.join_classes_set.all().order_by('-id'), page)
        else:
            return redirect('explore/')
        return render(request, 'class_op/join.html', data)
    return render(request, 'class_op/join.html')


def explore_classes(request):
    if request.method == "GET":
        page = request.GET.get('page')
        data = paginator_classes(models.Class.objects.all().order_by('-id'), page)
        return render(request, 'class_op/classes.html', data)
    return render(request, 'class_op/classes.html')


@mLogin_required
def mine_classes(request):
    if request.method == "GET":
        if not request.session.get('identity') == 'teacher':
            messages.error(request, '只有教师能访问 创建的课程 页面。')
            return render(request, 'class_op/classes.html')
        page = request.GET.get('page')
        user_id = request.session.get('user_id')
        user = User.objects.get(id=user_id)
        data = paginator_classes(user.own_classes_set.all().order_by('-id'), page)
        return render(request, 'class_op/mine_classes.html', data)
    return render(request, 'class_op/mine_classes.html')


@mLogin_required
def search_classes(request, type='none'):
    if request.method == "GET":
        page = request.GET.get('page')
        search = request.GET.get('search')
        user_id = request.session.get('user_id')
        user = User.objects.get(id=user_id)
        if len(search) == 0:
            messages.error(request, '搜索内容为空')
            return
        if len(search) >= 25:
            messages.errot(request, '搜索内容过长')
            return
        if type == 'none' or type == 'index':
            data = paginator_classes(models.Class.objects.filter(name__contains=search), page)
        elif type == 'mine':
            data = paginator_classes(user.own_classes_set.all().filter(name__contains=search).order_by('-id'), page)
        elif type == 'join':
            data = paginator_classes(user.join_classes_set.all().filter(name__contains=search).order_by('-id'), page)
        return render(request, 'class_op/components/classes_list.html', data)


@mLogin_required
def create_new_class(request):
    if request.POST:
        data = request.POST.dict()
        new_class = models.Class()
        new_class.name = data['new_class_name']
        new_class.describe = data['new_class_describe']
        new_class.cover = data['new_class_cover']
        try:
            new_class.save()
        except:
            return HttpResponse('fail')
        else:
            user_id = request.session.get('user_id')
            user = User.objects.get(id=user_id)
            new_class.teachers.add(user)
            try:
                new_class.save()
            except:
                messages.error(request, "创建新课程时发生错误")
                return HttpResponse("fail")
            else:
                messages.success(request, "创建新课程成功")
                return HttpResponse("done")
    else:
        return render(request, 'class_op/index.html')


@mLogin_required
def class_info(request, id):
    data = {}
    c = models.Class.objects.get(id=id)
    data["class"] = generate_class_ob(c)

    user_id = request.session.get('user_id')
    user = User.objects.get(id=user_id)
    if user in c.students.all():
        data["is_member"] = True
    else:
        data["is_member"] = False
    if user in c.teachers.all():
        data["is_creator"] = True
    else:
        data["is_creator"] = False
    return render(request, 'class_op/class_info.html', data)


@mLogin_required
def class_content(request, id, type):
    c = models.Class.objects.get(id=id)
    data = {}
    if type == 'info':
        data["class"] = generate_class_ob(c)
        return render(request, 'class_op/components/class_content_info.html', data)
    elif type == 'chapter':
        # POST method
        if request.method == 'POST':
            chapters_string = request.POST.dict()['chapters_string']
            c.chapter = chapters_string
            c.save()
            return class_info(request, c.id)
        # GET method
        else:
            chapters_list = []
            chapters = c.chapter.split('_._')
            chapter_sources_map = {}
            for chapter in chapters:
                chapter_data = chapter[3:].split('_:_')
                chapter_text = chapter_data[0]
                chapter_sources = []
                if len(chapter_data) > 1:
                    chapter_sources = chapter_data[1].split(',')
                    for source in chapter_sources:
                        test_source = c.test_set.filter(name=source)
                        if test_source:
                            chapter_sources_map[source] = test_source[0].id
                        else: # file source
                            chapter_sources_map[source] = source
                if chapter == '':
                    break
                elif chapter[0:3] == '<0>':
                    chapters_list.append({'type': 0,
                                          'data': chapter_text,
                                          'source': chapter_sources})
                elif chapter[0:3] == '<1>':
                    chapters_list.append({'type': 1,
                                          'data': chapter_text,
                                          'source': chapter_sources})
                elif chapter[0:3] == '<2>':
                    chapters_list.append({'type': 2,
                                          'data': chapter_text,
                                          'source': chapter_sources})
            data["chapters"] = chapters_list
            data["chapters"] = json.dumps(chapters_list)
            data["chapters_source_map"] = json.dumps(chapter_sources_map)

            data["tests"] = c.test_set.all()
            files = c.files.split(',')
            if files[0] == '':
                data["files"] = files[1:]
            else:
                data["files"] = files
            print(data)

            return render(request, 'class_op/components/class_content_chapter.html', data)
    elif type == 'source':
        data['class'] = c
        # 获取课程中文件
        count = 0
        set4 = []
        tests = []
        for t in c.test_set.all().order_by('-id'):
            test_ob = {
                "id": t.id,
                "name": t.name,
                "cover": (t.cover + '?w=220&h=160'),
            }
            set4.append(test_ob)
            if count < 3:
                count += 1
            else:
                tests.append(set4)
                count = 0
                set4 = []
        if count != 0:
            tests.append(set4)
        data["tests"] = tests
        # 为了能够从练习中返回 待完成
        request.session["current_class"] = c.id

        data['files'] = []
        files_list = c.files.split(',')
        for file in files_list:
            if len(file) > 0:
                data['files'].append(file)
        return render(request, 'class_op/components/class_content_source.html', data)

    elif type == 'exam':
        return HttpResponse('<p>This is exam</p>')
    elif type == 'discuss':
        return HttpResponse('<p>This is discuss</p>')
    elif type == 'notice':
        return HttpResponse('<p>This is Notice</p>')


@mLogin_required
def delete_class(request, id):
    user_id = request.session.get('user_id')
    u = User.objects.get(id=user_id)
    c = models.Class.objects.get(id=id)
    if u in c.teachers.all():
        try:
            c.delete()
        except Exception as error:
            messages.error(request, "删除课程出错:")
            return redirect('/classes/' + id + '/')
        messages.info(request, "删除课程成功")
        return redirect('/classes/')
    else:
        messages.error(request, "你不是当前课程的创建者，无法删除")
        return redirect('/classes/' + id + '/')


@mLogin_required
def join_class(request, id):
    c = models.Class.objects.get(id=id)
    user_id = request.session.get('user_id')
    u = models.User.objects.get(id=user_id)
    c.students.add(u)
    return redirect('/classes/' + id + '/')


@mLogin_required
def quit_class(request, id):
    c = models.Class.objects.get(id=id)
    user_id = request.session.get('user_id')
    u = models.User.objects.get(id=user_id)
    c.students.remove(u)
    return redirect('/classes/' + id + '/')


@mLogin_required
def edit_class(request, id):
    c = models.Class.objects.get(id=id)
    user_id = request.session.get('user_id')
    u = models.User.objects.get(id=user_id)
    if u not in c.teachers.all():
        messages.add_message(request, messages.INFO, '你不是该课程的授课教师')
        return redirect('/classes/' + id + '/')
    data = {}
    if request.method == 'GET':
        data["class"] = generate_class_ob(c)
        tests = Test.objects.all().order_by('-id')
        data["tests"] = tests
        own_tests = c.test_set.all()
        exist_tests = []
        for t in own_tests:
            exist_tests.append(t.id)
        data["exist_tests"] = json.dumps(exist_tests)
        return render(request, 'class_op/edit_class.html', data)
    elif request.method == 'POST':
        if request.POST:
            data = request.POST.dict()
            c.name = data['edit_class_name']
            c.describe = data['edit_class_describe']
            c.save()
            t_id_list = data['edit_class_tests'].split(',')
            c.test_set.clear()
            if t_id_list != ['']:
                for t_id in t_id_list:
                    if t_id == '':
                        break
                    t = Test.objects.get(id=t_id)
                    c.test_set.add(t)
            return redirect('/classes/' + id + '/')
    return render(request, 'class_op/edit_class.html')


def classes(request):
    data = {}
    classes = models.Class.objects.all()
    classes_information = []
    for single_class in classes:
        class_information = {
            "class_name": single_class.class_name,
            "create_time": single_class.create_time,
            "students_number": single_class.students_number
        }
        classes_information.append(class_information)
    data["classes_information"] = classes_information
    return render(request, 'class_op/classes.html', data)


@mLogin_required
def do_test(request, class_id, test_id):
    t = Test.objects.get(id=test_id)
    request.session["do_test_name"] = t.name
    request.session["do_test_class"] = class_id
    return render(request, "do_test.html")

#
# @mLogin_required
# def tests_list(request, class_id):
#     c = models.Class.objects.get(id=class_id)
#     # 获取课程中练习
#     tests = []
#     for t in c.test_set.all().order_by('-id'):
#         test_ob = {
#             "id": t.id,
#             "name": t.name,
#             "cover": (t.cover + '?w=220&h=160'),
#         }
#         tests.append(test_ob)
#     # 为了能够从练习中返回 待完成
#     request.session["current_class"] = c.id
#     return HttpResponse(json.dumps(tests))
