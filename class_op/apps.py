from django.apps import AppConfig


class ClassOpConfig(AppConfig):
    name = 'class_op'
