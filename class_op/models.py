from django.db import models
from login.models import User
# Create your models here.


class Class(models.Model):
    name = models.CharField(max_length=64, unique=True)
    create_time = models.DateField(auto_now_add=True)
    teachers = models.ManyToManyField(User, related_name='own_classes_set')
    students = models.ManyToManyField(User, related_name='join_classes_set')
    cover = models.CharField(max_length=128, blank=True)
    chapter = models.CharField(max_length=4096, blank=True)
    files = models.CharField(max_length=1280)
    describe = models.CharField(max_length=1280, blank=True)


class notice(models.Model):
    _class = models.ForeignKey(Class, on_delete=models.CASCADE)
    detail = models.CharField(max_length=128, blank=True)
    publisher = models.ForeignKey(User, on_delete=models.CASCADE, related_name="publish_notice_set")
    receiver = models.ManyToManyField(User, related_name="receive_notice_set", null=True)
