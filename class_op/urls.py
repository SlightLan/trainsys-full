from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from class_op import class_op_views

urlpatterns = [
    path('', class_op_views.index),
    path('join/', class_op_views.joined_classes),
    path('create_new_class/', class_op_views.create_new_class),
    path('index/', class_op_views.index),
    path('mine/', class_op_views.mine_classes),
    url(r'(join|mine|index)/search/$', class_op_views.search_classes),
    url(r'search/$', class_op_views.search_classes),
    url(r'(\d+)/content_(\w+)/', class_op_views.class_content),
    # url(r'(\d+)/tests_list/', class_op_views.tests_list),
    url(r'(\d+)/join/', class_op_views.join_class),
    url(r'(\d+)/quit/', class_op_views.quit_class),
    url(r'(\d+)/delete/', class_op_views.delete_class),
    url(r'(\d+)/edit/', class_op_views.edit_class),
    url(r'(\d+)/do_test/(\d+)', class_op_views.do_test),
    url(r'(\d+)/', class_op_views.class_info, name='class_info'),
]
