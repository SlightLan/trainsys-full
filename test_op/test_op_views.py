from django.shortcuts import render, HttpResponse, redirect
from . import models
from class_op.models import Class
from login.models import User
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import TemplateView

from TrainSys.global_func import mLogin_required


def paginator_tests(tests, page):
    count = 0
    set4 = []
    all_tests = []
    data = {}
    for t in tests:
        test_ob = {
            'id': t.id,
            'name': t.name,
            'cover': (t.cover + '?w=250&h=200'),
        }
        set4.append(test_ob)
        if count < 3:
            count += 1
        else:
            all_tests.append(set4)
            count = 0
            set4 = []
    if count != 0:
        all_tests.append(set4)
    paginator = Paginator(all_tests, 16)
    if page:
        try:
            page = int(page)
            page_data = paginator.page(page)
        except PageNotAnInteger:
            page_data = paginator.page(1)
        except EmptyPage:
            page_data = paginator.page(paginator.num_pages)
        data['page_data'] = page_data
    else:
        page_data = paginator.page(1)
        data['page_data'] = page_data
    return data


def index(request):
    if request.method == 'GET':
        page = request.GET.get('page')
        data = paginator_tests(models.Test.objects.all().order_by('-id'), page)
        return render(request, 'test_op/index.html', data)
    return render(request, 'test_op/index.html')


@mLogin_required
def mine(request):
    if request.method == 'GET':
        if not request.session.get('identity') == 'teacher':
            messages.error(request, '只有教师能访问 创建的练习 页面。')
            return render(request, 'test_op/index.html')
        page = request.GET.get('page')
        user_id = request.session.get('user_id')
        user = User.objects.get(id=user_id)
        data = paginator_tests(user.own_tests_set.all().order_by('-id'), page)
        return render(request, 'test_op/mine.html', data)
    return render(request, 'test_op/mine.html')


@mLogin_required
def joined(request):
    if request.method == 'GET':
        page = request.GET.get('page')
        user_id = request.session.get('user_id')
        user = User.objects.get(id=user_id)
        data = paginator_tests(user.join_tests_set.all().order_by('-id'), page)
        return render(request, 'test_op/join.html', data)
    return render(request, 'test_op/join.html')


@mLogin_required
def search_tests(request, type='none'):
    if request.method == "GET":
        page = request.GET.get('page')
        search = request.GET.get('search')
        user_id = request.session.get('user_id')
        user = User.objects.get(id=user_id)
        if len(search) == 0:
            messages.error(request, '搜索内容为空')
            return
        if len(search) >= 25:
            messages.errot(request, '搜索内容过长')
            return
        if type == 'none' or type == 'index':
            data = paginator_tests(models.Test.objects.filter(name__contains=search), page)
        elif type == 'mine':
            data = paginator_tests(user.own_tests_set.all().filter(name__contains=search).order_by('-id'), page)
        elif type == 'join':
            data = paginator_tests(user.join_tests_set.all().filter(name__contains=search).order_by('-id'), page)
        return render(request, 'test_op/components/test_list.html', data)



def create_new_test_operation(request):
    if not request.session.get('identity') == 'teacher':
        messages.add_message(request, messages.INFO, '只有教师可以创建练习！')
        return redirect('tests/index.html')
    if request.POST:
        data = request.POST.dict()
        test = models.Test()
        test.name = data["new_test_name"]
        if data["new_test_cover"] == "":
            test.cover = ""
        else:
            test.cover = data["new_test_cover"]
        test.describe = data["new_test_describe"]
        user_id = request.session.get('user_id')
        test.teacher = User.objects.get(id=user_id)
        test.save()

        class_list = data["create_test_classes"]
        class_list = class_list.split(',')
        if class_list != ['']:
            for c_id in class_list:
                c = Class.objects.get(id=c_id)
                test.classes.add(c)
        try:
            test = models.Test.objects.get(name=data["new_test_name"])
            return test
        except Exception as error:
            return None


@mLogin_required
def create_new_test_edit(request):
    test = create_new_test_operation(request)
    if test:
        request.session["test_name"] = test.name
    else:
        messages.error("创建练习失败,请重试")
        return redirect('/tests/')
    return render(request, "create_test.html")


@mLogin_required
def create_new_test(request):
    test = create_new_test_operation(request)
    if test:
        return redirect('/tests/' + str(test.id))
    else:
        messages.error("创建练习失败,请重试")
        return redirect('/tests/')


def test_info_data(request, id):
    data = {}
    t = models.Test.objects.get(id=id)
    test_ob = {
        'id': t.id,
        'name': t.name,
        'create_time': t.create_time,
        'describe': t.describe,
        'empty_test': '',
        'teacher': t.teacher,
        'cover': (t.cover + '?w=250&h=200'),
    }
    if len(t.describe) == 0:
        test_ob['describe'] = "教师还没有为这个练习编写描述信息。"
    if t.data == '':
        test_ob['empty_test'] += "\n\n\n 注意 : 这是一个空练习，教师还没有编辑它的内容。"
    data['test'] = test_ob
    user_id = request.session.get('user_id')
    u = User.objects.get(id=user_id)
    if u == t.teacher:
        data['is_creator'] = True
    else:
        data['is_creator'] = False
    return data


@mLogin_required
def test_info(request, id):
    data = test_info_data(request, id)
    return render(request, 'test_op/test_info.html', data)


@mLogin_required
def test_info_from_class(request, id, class_id):
    data = test_info_data(request, id)
    c = Class.objects.get(id=class_id)
    data['class_name'] = c.name
    data['class_id'] = c.id
    return render(request, 'test_op/test_info.html', data)

@mLogin_required
def edit_test_info(request, id):
    user_id = request.session.get('user_id')
    u = User.objects.get(id=user_id)
    t = models.Test.objects.get(id=id)
    if not u == t.teacher:
        messages.error(request, "你不是当前练习的创建者，无法编辑该练习")
        return redirect('/tests/' + id + '/')
    else:
        if request.POST:
            data = request.POST.dict()
            print(data)
            test = models.Test.objects.get(id=id)
            test.name = data["edit_test_name"]
            test.cover = data["edit_test_cover"]
            test.describe = data["edit_test_describe"]
            test.save()
            try:
                test = models.Test.objects.get(name=data["edit_test_name"])
            except Exception as error:
                messages.error("修改练习失败,请重试")
            else:
                messages.info(request, "修改练习成功！")
                return redirect('/tests/' + id + '/')


@mLogin_required
def edit_test(request, id):
    user_id = request.session.get('user_id')
    u = User.objects.get(id=user_id)
    t = models.Test.objects.get(id=id)
    if not u == t.teacher:
        messages.error(request, "你不是当前练习的创建者，无法编辑该练习")
        return redirect('/tests/' + id + '/')
    else:
        request.session["test_name"] = t.name
        return render(request, "create_test.html")


@mLogin_required
def delete_test(request, id):
    user_id = request.session.get('user_id')
    u = User.objects.get(id=user_id)
    t = models.Test.objects.get(id=id)
    if not u == t.teacher:
        messages.error(request, "你不是当前练习的创建者，无法删除该练习")
        return redirect('/tests/' + id + '/')
    else:
        try:
            t.delete()
        except Exception as error:
            messages.error(request, "删除练习出错:" + error)
            return redirect('/tests/' + id + '/')
        messages.info(request, "删除练习成功")
        return redirect('/tests/')


# for users entered from class page
@mLogin_required
def do_test(request, id):
    t = models.Test.objects.get(id=id)
    request.session["do_test_name"] = t.name
    if len(t.data) < 10:
        messages.error(request, "练习暂无内容，请联系教师编辑联系内容")
        cur_class_url = request.session.get("cur_class_url")
        return redirect(cur_class_url)
    return redirect('/main_session/do_test/')
