from django.urls import path
from django.conf.urls import url
from test_op import test_op_views

urlpatterns = [
    path('', test_op_views.index),
    path('index/', test_op_views.index),
    path('mine/', test_op_views.mine),
    path('join/', test_op_views.joined),
    path('create_new_test/', test_op_views.create_new_test),
    path('create_new_test_edit/', test_op_views.create_new_test_edit),
    url(r'(index|mine|join)/search/$', test_op_views.search_tests),
    url(r'search/$', test_op_views.search_tests),
    url(r'^(\d+)/delete/', test_op_views.delete_test),
    url(r'^(\d+)/edit/', test_op_views.edit_test),
    url(r'^(\d+)/edit_info/', test_op_views.edit_test_info),
    url(r'^(\d+)/(\d+)/', test_op_views.test_info_from_class, name='test_info_from_class'),
    url(r'^(\d+)/', test_op_views.test_info, name='test_info'),
    url(r'^class/(\d+)/', test_op_views.do_test, name='do_test'),
]