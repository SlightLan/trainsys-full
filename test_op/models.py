from django.db import models
from login.models import User
from class_op.models import Class
# Create your models here.


class Test(models.Model):
    name = models.CharField(max_length=64, unique=True)
    create_time = models.DateField(auto_now_add=True)
    describe = models.CharField(max_length=512, blank=True)
    data = models.TextField(max_length=1000000, blank=True)
    teacher = models.ForeignKey(User, on_delete=models.CASCADE, related_name='own_tests_set')
    students = models.ManyToManyField(User, related_name='join_tests_set')
    classes = models.ManyToManyField(Class, through='ClassCrossTest')
    cover = models.CharField(max_length=128, blank=True)


class ClassCrossTest(models.Model):
    _class = models.ForeignKey(Class, on_delete=models.CASCADE)
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    chapter = models.CharField(max_length=128, blank=True)
    date = models.DateField(auto_now_add=True)


class TestPage(models.Model):
    test = models.ForeignKey('Test', on_delete=models.CASCADE, default='')
    page = models.IntegerField(default=0)
    title = models.CharField(max_length=64)


class TestPageElement(models.Model):
    test_page = models.ForeignKey('TestPage', on_delete=models.CASCADE, default='')
    id = models.IntegerField(unique=True, primary_key=True)
    attributes = models.CharField(max_length=1024)
