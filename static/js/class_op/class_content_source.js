$(document).ready(() => {
    let class_id = $('.class-info-title').data('id');
    //课程详情页面的查看练习详情
    $(".open-test-detail").click(function () {
        //首先解绑开始练习的点击事件
        $('#do_test_btn').unbind('click');
        let id = $(this).parent()[0].dataset.id;
        let if_has_data = false;
        $.ajax({
            url: "/api/find_test_by_id/" + id + "/",
            type: "GET",
            data: $("#new_class_form").serialize(),
        }).done(function (response) {
            response = eval(response);
            console.log(response);
            if (response[0].cover == "") {
                $("#test_detail_cover").attr("src", "/static/img/default_cover_small.jpg");
            } else {
                $("#test_detail_cover").attr("src", response[0].cover + "?w=125&h=100");
            }
            $("#test_detail_name").text(response[0].name);
            $("#test_detail_teacher").text(response[0].teacher);
            if (response[0].describe.length == 0) {
                response[0].describe = "教师还没有为练习添加描述";
            }
            $("#test_detail_describe").html(response[0].describe);
            response[0].data && (if_has_data = true);
            //练习详情页面，点击按钮开始做练习
            $('#do_test_btn').click(function () {
                if (if_has_data) {
                    window.location.href = "do_test/" + id + "/" + class_id + "/";
                } else {
                    window.alert("该练习尚无内容，请联系教师");
                }
            });
            $('#jump_to_test_btn').click(function () {
                window.location.href = "/tests/" + id + "/" + class_id + "/";
            })
        })

    });
})

