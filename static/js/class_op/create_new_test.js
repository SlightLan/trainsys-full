$(document).ready(() => {
    let img_uploader = $("#new_test_img_uploader");
    let preview = $("#new_test_img_upload_btn");
    let url = "/tests/create_new_test/";
    let handle_method = 'refresh';
    preview.click(function (event) {
        event.preventDefault();
        return img_uploader.click();
    });
    img_uploader.bind('change', updateImageDisplay);
    $("input[name=create_test_classes]").val($("#create_test_btn").attr("data-class-id"));
    $("#create_test_btn").click(function () {
        $("#create_test_classes").next().css("background-color", "#ffffff");
    })
    $("#new_test_form").submit((e) => {
        e.preventDefault();
        let data = {
            'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val(),
            'new_test_name': $("input[name=new_test_name]").val(),
            'create_test_classes': $("input[name=create_test_classes]").val(),
            'new_test_describe': $("textarea[name=new_test_describe]").val(),
            'new_test_cover': $("input[name=new_test_cover]").val(),
        };
        $.ajax({
            url: url,
            type: 'POST',
            data: data
        }).done(function (response, status, xhr) {
            console.log(response);
            if (status === 'success' && xhr.status === 200) {
                if (handle_method === 'refresh') {
                    window.alert('创建练习成功！');
                    location.reload();
                } else if (handle_method === 'edit') {
                    let select = confirm('创建练习成功，前往练习内容编辑页面将丢失当前编辑的课程信息，是否确定？');
                    if (select) {
                        window.location.href = '/main_session/create_test';
                    } else {
                        // do nothing
                    }
                }
            } else {
                window.alert('创建练习过程中发生错误！');
            }
        }).fail(function (response) {
            window.alert('创建练习过程中发生错误！\n请检查是否输入了重复的练习名称。');
        });
    });
    $("#new_test_submit").click(function () {
        handle_method = 'refresh';
        url = "/tests/create_new_test/";
        $("#new_test_form").submit();
    });
    $("#new_test_submit_edit").click(function () {
        handle_method = 'edit';
        url = "/tests/create_new_test_edit/";
        $("#new_test_form").submit();
    });
});

function updateImageDisplay() {
    let img_uploader = document.getElementById('new_test_img_uploader');
    let preview = document.getElementById("new_test_img_upload_btn");
    let img_server = document.getElementById('new_test_img_upload_btn').getAttribute('url');
    let curFile = img_uploader.files[0];
    let formData = new FormData();
    formData.append('file', curFile);
    $.ajax({
        url: img_server + "upload/",
        type: "POST",
        contentType: false,
        processData: false,
        data: formData,
    }).done(function (response) {
        response = response.replace(/\n/g, '');
        response = response.replace(/ /g, '');
        let regex = /MD5:(.*)(?=<\/h1>)/g;
        let url = img_server + regex.exec(response)[1];
        document.getElementById("new_test_cover").value = url;
    });
    let url = URL.createObjectURL(curFile);
    preview.style.backgroundImage = "url(" + url + ")";
}