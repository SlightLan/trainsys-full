$(document).ready(() => {
    let chapters_list = $('#class_content_chapter_data').val();
    let chapters_list_backup = JSON.parse(chapters_list);

    chapters_list = JSON.parse(chapters_list);
    let chapters_source_map = $('#class_content_chapter_source_map').val();
    chapters_source_map = JSON.parse(chapters_source_map);

    let chapter_edit_modal = $('#chapter_edit_modal');
    let confirm_btn = $('#edit_class_content_confirm');
    let cancel_btn = $('#edit_class_content_cancel');
    let edit_btn = $('#edit_class_content_chapter');

    setChapters()

    edit_btn.click(function () {
        confirm_btn.css('display', 'block');
        cancel_btn.css('display', 'block');
        edit_btn.css('display', 'none');

        $('.chapter-add').unbind('click');
        $('.chapter-edit').unbind('click');
        $('.chapter-delete').unbind('click');

        for (let n = 0; n < $('.chapter-title').length; n++) {
            let title = $($('.chapter-title')[n]);
            if (title.children().length > 0) {
                let edit_el = $(
                    '<button class="chapter-additional-el chapter-delete class-content-chapter-inline-btn">删除</button> ' +
                    '<button class="chapter-additional-el chapter-edit class-content-chapter-inline-btn">编辑</button> ' +
                    '<button class="chapter-additional-el chapter-add class-content-chapter-inline-btn">添加</button> ' +
                    '<div class="chapter-additional-el" style="border-bottom: dashed 1px #909090;"></div>'
                );
                edit_el.insertBefore(title.children()[0]);
            } else {
                title.append(
                    '<button class="chapter-additional-el chapter-delete class-content-chapter-inline-btn">删除</button> ' +
                    '<button class="chapter-additional-el chapter-edit class-content-chapter-inline-btn">编辑</button> ' +
                    '<button class="chapter-additional-el chapter-add class-content-chapter-inline-btn">添加</button> ' +
                    '<div class="chapter-additional-el" style="border-bottom: dashed 1px #909090;"></div>'
                );
            }
        }

        $('.chapter-edit').click(function () {
            let position = $(this).offset();
            let modal_dialog = chapter_edit_modal.children()[0];
            let modal_input_title = $('#chapter_edit_modal_input_title');
            let modal_confirm_btn = $('#chapter_edit_modal_confirm');
            let data_id = $(this).parent()[0].dataset['id'];
            modal_dialog.style.left = (position.left - 180) + 'px';
            modal_dialog.style.top = (position.top + 20) + 'px';
            modal_input_title.val(chapters_list[data_id].data);

            let select = $('#chapter_edit_modal_select_source');
            if (chapters_list[data_id].source.length > 0) {
                let select_source_list = []
                for(let n = 0; n < chapters_list[data_id].source.length; n++){
                    select_source_list.push(chapters_source_map[chapters_list[data_id].source[n]]);
                }
                select.selectpicker('val', select_source_list);
            }else{
                select.selectpicker();
            }

            modal_confirm_btn.click(function () {
                chapters_list[data_id].data = modal_input_title.val();
                if(select.val().lengtn > 0){
                    chapters_list[data_id].source = select.val().split(',');
                }else{
                    chapters_list[data_id].source = [];
                }

                console.log(chapters_list);
                setChapters();
                $('#edit_class_content_chapter').click();
                modal_confirm_btn.unbind('click');
            });

            chapter_edit_modal.modal('show');
        });

        $('.chapter-add').click(function () {
            let parent = $($(this).parent()[0]);

        })
    });

    cancel_btn.click(function () {
        confirm_btn.css('display', 'none');
        cancel_btn.css('display', 'none');
        edit_btn.css('display', 'block');
        chapters_list = chapters_list_backup;
        setChapters();
    });

    confirm_btn.click(function () {
        confirm_btn.css('display', 'none');
        cancel_btn.css('display', 'none');
        edit_btn.css('display', 'block');
        let chapter_titles = $('.chapter-title');
        for (let n = 0; n < chapter_titles.length; n++) {
            $(chapter_titles[n]).children('.chapter-additional-el').remove();
        }
        let chapters_string = '';
        for (let n in chapters_list) {
            chapters_string += ('<' + chapters_list[n].type + '>');
            chapters_string += chapters_list[n].data;
            chapters_string += '_._';
        }
        let data = {
            'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val(),
            'chapters_string': chapters_string,
        }
        $.ajax({
            url: 'content_chapter/',
            type: 'POST',
            data: data,
        });
    });

    $('.class-content-chapter-source-btn').click(function () {
        $('#do_test_btn').unbind('click');
        $('#show_test_detail_modal').modal('show');
        let id = $(this).data('id');
        let if_has_data = false;
        let class_id = $('.class-info-title').data('id');

        $.ajax({
            url: "/api/find_test_by_id/" + id + "/",
            type: "GET",
            data: $("#new_class_form").serialize(),
        }).done(function (response) {
            response = JSON.parse(response);
            console.log(response);
            if (response[0].cover == "") {
                $("#test_detail_cover").attr("src", "/static/img/default_cover_small.jpg");
            } else {
                $("#test_detail_cover").attr("src", response[0].cover + "?w=125&h=100");
            }
            $("#test_detail_name").text(response[0].name);
            $("#test_detail_teacher").text(response[0].teacher);
            if (response[0].describe.length == 0) {
                response[0].describe = "教师还没有为练习添加描述";
            }
            $("#test_detail_describe").html(response[0].describe);
            response[0].data && (if_has_data = true);
            //练习详情页面，点击按钮开始做练习
            $('#do_test_btn').click(function () {
                if (if_has_data) {
                    window.location.href = "do_test/" + id + "/" + class_id + "/";
                } else {
                    window.alert("该练习尚无内容，请联系教师");
                }
            });
            $('#jump_to_test_btn').click(function () {
                window.location.href = "/tests/" + id + "/" + class_id + "/";
            })
        })
    })


    function setChapters() {
        $('#class_content_chapter_tree').empty();
        let chineseNum = ['', '一', '二', '三', '四', '五', '六', '七', '八', '九'];
        let chineseNumUnit = ['', '十'];
        let record = [1, 1, 1];
        let html = '';
        for (let n in chapters_list) {
            let type = chapters_list[n].type;
            let data = chapters_list[n].data;
            let source = chapters_list[n].source;
            if (type === 0) {
                let number = record[0] < 10 ? chineseNum[record[0]] :
                    record[0] < 20 ? chineseNumUnit[1] + chineseNum[record[0] - 10] :
                        chineseNum[Math.floor(record[0] / 10)] + chineseNumUnit[1] + chineseNum[record[0] % 10];
                number += '、';
                html = html + '<h3 class="chapter-title" data-id="' + n + '">' + number + data + '\n';
                for (let m = 0; m < source.length; m++) {
                    if (source[m]) {
                        html = html + '<button class="class-content-chapter-source-btn" data-id="' + chapters_source_map[source[m]] + '">' + source[m] + '</button>';
                    }
                }
                html += '</h3>\n';
                record[0] += 1;
                record[1] = 1;
                record[2] = 1;
            } else if (type === 1) {
                let number = record[1] + '、';
                html = html + '<h4 class="chapter-title" style="margin-left: 4%;" data-id="' + n + '">' + number + data + '\n';
                for (let m = 0; m < source.length; m++) {
                    if (source[m]) {
                        html = html + '<button class="class-content-chapter-source-btn" data-id="' + chapters_source_map[source[m]] + '">' + source[m] + '</button>';
                    }
                }
                html += '</h4>\n';
                record[1] += 1;
                record[2] = 1;
            } else if (type === 2) {
                let number = (record[1] - 1) + '.' + record[2] + ' ';
                html = html + '<h5 class="chapter-title" style="margin-left: 8%;" data-id="' + n + '">' + number + data + '\n';
                for (let m = 0; m < source.length; m++) {
                    if (source[m]) {
                        html = html + '<button class="class-content-chapter-source-btn" data-id="' + chapters_source_map[source[m]] + '">' + source[m] + '</button>';
                    }
                }
                html += '</h5>\n';
                record[2] += 1;
            }
        }
        $('#class_content_chapter_tree').append(html);
    }
})

