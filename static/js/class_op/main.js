$(document).ready(() => {
    let class_id = $('.class-info-title').data('id');
    // 创建新课程函数
    let img_uploader = $("#new_class_img_uploader");
    let preview = $("#new_class_img_upload_btn");
    preview.click(function (event) {
        event.preventDefault();
        return img_uploader.click();
    });
    img_uploader.bind('change', updateImageDisplay);

    $("#new_class_submit").click(function () {
        $("#new_class_form").submit();
    });

    $("#new_class_form").submit(function (event) {
        $.ajax({
            url: "/classes/create_new_class/",
            type: "POST",
            data: $("#new_class_form").serialize(),
        }).done(function (response) {
            $('#create_new_class_modal').modal('hide');
            if (response === 'done') {
                window.location.href = "/classes/";
                // let el = document.createElement("div");
                // el.className = "alert alert-success";
                // el.style.textAlign = 'center';
                // el.innerHTML = "创建新课程成功!";
                // $('body').prepend(el)
                // setTimeout(function () {
                //     $('.alert').remove();
                // }, 5000);
            } else {
                // let el = document.createElement("div");
                // el.className = "alert alert-danger";
                // el.style.textAlign = 'center';
                // el.innerHTML = "创建新课程失败!";
                // $('body').prepend(el)
                // setTimeout(function () {
                //     $('.alert').remove();
                // }, 5000);
            }
        });
        return false;
    });
    // // 课程按钮hover函数
    // $(".class-info, .test-info").hover(function (event) {
    //     if (event.type == 'mouseenter') {
    //         event.currentTarget.style.boxShadow = "0 0 12px rgba(0,0,0,.3)";
    //     } else {
    //         event.currentTarget.style.boxShadow = "0 0 12px rgba(0,0,0,.1)";
    //     }
    // });
    // 以更新页面布局，不需要再设置窗口大小变化了函数了
    // //最开始时检测窗口大小并决定布局
    // if(document.documentElement.clientWidth < 1500){
    //         document.getElementsByClassName('adjust-container-div')[0].className = "adjust-container-div";
    // }
    // // 调整窗口大小时改变布局函数
    // window.onresize = function(){
    //     if(document.documentElement.clientWidth < 1500){
    //         document.getElementsByClassName('adjust-container-div')[0].className = "adjust-container-div";
    //     }else{
    //         document.getElementsByClassName('adjust-container-div')[0].className = "adjust-container-div col-xs-8 col-xs-offset-2";
    //     }
    // }
    //给edit界面使用的select初始化
    $("#edit_class_tests").change(function () {
        $("input[name=edit_class_tests]").val($("#edit_class_tests").val())
    });

    // 点击课程标题返回详情页面
    $('.class-info-title').click(function () {
        window.location.href = '/classes/' + $('.class-info-title').data('id');
    });
    //搜索课程功能
    $('#search_btn_class').click(function () {
        let search = $('#search_input_class').val();
        if (search.length > 25) {
            window.alert('搜索内容过长！');
            return;
        }
        if (search.length === 0) {
            window.alert('请输入搜索关键字！');
        }
        $.ajax({
            url: 'search/',
            type: 'GET',
            data: {
                'search': search,
            },
            timeout: 1000,
        }).done(function (response) {
            let parent = $('.class-list').parent();
            $('.class-list').remove();
            parent.append(response);
        }).fail(function (response, status) {
            if (status === 'timeout') {
                window.alert("获取搜索结果超时，请检查网络后重试。");
            } else {
                window.alert("获取搜索结果失败，请检查网络后重试。");
            }
        });

    })
    if ($('#upload_files_input').length > 0) {
        $('#upload_files_input').fileinput({
            language: "zh",//配置语言
            showUpload: true, //显示整体上传的按钮
            showRemove: true,//显示整体删除的按钮
            uploadAsync: true,//默认异步上传
            uploadLabel: "上传",//设置整体上传按钮的汉字
            removeLabel: "移除",//设置整体删除按钮的汉字
            uploadClass: "btn btn-primary",//设置上传按钮样式
            showCaption: true,//是否显示标题
            dropZoneEnabled: true,//是否显示拖拽区域
            uploadUrl: '/api/upload_file/',//这个是配置上传调取的后台地址，本项目是SSM搭建的
            maxFileSize: 102400,//文件大小限制kb
            maxFileCount: 20,//允许最大上传数，可以多个，
            enctype: 'multipart/form-data',
            allowedFileExtensions: ["pdf", "jpg", "png", "gif", "doc", "docx", "zip", "xlsx", "txt", "ppt", "pptx"],/*上传文件格式限制*/
            msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
            showBrowse: true,
            browseOnZoneClick: true,
            uploadExtraData: {
                'class_id': class_id,
            },
            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            },
            ajaxSettings: {
                headers: {'X-CSRFToken': $.cookie('csrftoken')},
            }
        });
    }
    if($("a[id='class_content_info']").length > 0){
        $.ajax({
            url: 'content_info/',
            type: 'GET',
            timeout: 1000,
        }).done(function (response) {
            $("a[id='class_content_info']").addClass('class-selected-tab');
            let parent = $('#class_content_container');
            parent.empty();
            parent.append(response);
        }).fail(function (response, status) {
            if (status === 'timeout') {
                window.alert("获取课程信息页面超时，请检查网络后重试");
            } else {
                window.alert("获取课程信息页面失败，请检查网络后重试。");
            }
        });
    }
    $("a[class='class-content-tab']").click(function () {
        $("a[class='class-content-tab class-selected-tab']").removeClass('class-selected-tab');
        $(this).addClass('class-selected-tab');
        let type = this.dataset['target'];
        console.log(type);
        $.ajax({
            url: 'content_' + type + '/',
            type: 'GET',
            timeout: 1000,
        }).done(function (response) {
            let parent = $('#class_content_container');
            parent.empty();
            parent.append(response);
        }).fail(function (response, status) {
            if (status === 'timeout') {
                window.alert("获取指定页面超时，请检查网络后重试");
            } else {
                window.alert("获取指定页面失败，请检查网络后重试。");
            }
        });
    });


    function updateImageDisplay() {
        let img_uploader = document.getElementById('new_class_img_uploader');
        let preview = document.getElementById("new_class_img_upload_btn");
        let curFile = img_uploader.files[0];
        let formData = new FormData();
        let img_server = document.getElementById('new_class_img_upload_btn').getAttribute('url');
        formData.append('file', curFile);
        $.ajax({
            url: img_server + 'upload/',
            type: "POST",
            contentType: false,
            processData: false,
            data: formData,
        }).done(function (response) {
            console.log(response);
            response = response.replace(/\n/g, '');
            response = response.replace(/ /g, '');
            let regex = /MD5:(.*)(?=<\/h1>)/g;
            let url = img_server + regex.exec(response)[1];
            document.getElementById("new_class_cover").value = url;
        });
        let url = URL.createObjectURL(curFile);
        preview.style.backgroundImage = "url(" + url + ")";
    }

});