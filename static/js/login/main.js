$(document).ready(() => {
    $('#teacher_verify_trigger').click(() => {
        setTimeout(() => {
            $('#teacher_verify_code')[0].focus();
        }, 500);
    })
    $('#teacher_verify_submit').click(() => {
        $('#teacher_verify_form').submit();
    });
    $('#teacher_verify_form').submit(function () {
        $.ajax({
            url: "teacher_verify/",
            type: "POST",
            data: $("#teacher_verify_form").serialize(),
        }).done((response) => {
            if(response === 'done'){
                window.alert("验证成功");
            }else if(response === 'error'){
                window.alert("验证失败");
            }else {
                window.alert("验证过程发生错误，请重试")
            }
            $('#teacher_verify_modal').modal('hide');
        });
        return false;
    });
});