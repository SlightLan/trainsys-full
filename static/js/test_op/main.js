$(document).ready(() => {
    $('#delete_test_btn').click(function () {
        let select = confirm('注意:\n该操作将同时删除该练习相关的学习记录!\n确定删除该练习？');
        if(select){
            $.ajax({
                url: 'delete/',
                type: 'GET'
            }).done(function () {
               window.location.href = '/tests/';
            });
        }else{

        }
    });
    //搜索课程功能
    $('#search_btn_test').click(function () {
        let search = $('#search_input_test').val();
        if(search.length > 25){
            window.alert('搜索内容过长！');
            return;
        }
        if(search.length === 0){
            window.alert('请输入搜索关键字！');
        }
        $.ajax({
            url: 'search/',
            type: 'GET',
            data: {
                'search': search,
            },
            timeout: 1000,
        }).done(function (response) {
            let parent = $('.test-list').parent();
            $('.test-list').remove();
            parent.append(response);
        }).fail(function (response, status) {
            if(status === 'timeout'){
                window.alert("获取搜索结果超时，请检查网络后重试。");
            }else {
                window.alert("获取搜索结果失败，请检查网络后重试。");
            }
        })
    })
});


