$(document).ready(() => {
    let img_uploader = $("#edit_test_img_uploader");
    let preview = $("#edit_test_img_upload_btn");
    preview.click(function (event) {
        event.preventDefault();
        return img_uploader.click();
    });
    img_uploader.bind('change', updateImageDisplay);

    $("#edit_test_btn").click(function () {
        $("#edit_test_classes").next().css("background-color", "#ffffff");
    })
    $("#edit_test_submit").click(function () {
        $("#edit_test_form").attr('action', 'edit_info/').submit();
    })
    $("#edit_test_submit_and_edit").click(function () {
        $("#edit_test_form").attr('action', 'edit/').submit();
    })
});

function updateImageDisplay() {
    let img_uploader = document.getElementById('edit_test_img_uploader');
    let preview = document.getElementById("edit_test_img_upload_btn");
    let curFile = img_uploader.files[0];
    let img_server = document.getElementById('edit_test_img_upload_btn').getAttribute('url');
    console.log(curFile);
    let formData = new FormData();
    formData.append('file', curFile);
    $.ajax({
        url: img_server + "upload",
        type: "POST",
        contentType: false,
        processData: false,
        data: formData,
    }).done(function (response) {
        response = response.replace(/\n/g, '');
        response = response.replace(/ /g, '');
        let regex = /MD5:(.*)(?=<\/h1>)/g;
        let url = img_server + regex.exec(response)[1];
        document.getElementById("edit_test_cover").value = url;
    });
    let url = URL.createObjectURL(curFile);
    preview.style.backgroundImage = "url(" + url + ")";
}