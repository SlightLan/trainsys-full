$(document).ready(() => {
    let img_uploader = $("#new_test_img_uploader");
    let preview = $("#new_test_img_upload_btn");
    preview.click(function (event) {
        event.preventDefault();
        return img_uploader.click();
    });
    img_uploader.bind('change', updateImageDisplay);

    $("#create_test_btn").click(function () {
        $("#create_test_classes").next().css("background-color", "#ffffff");
    })
    $("#new_test_submit").click(function () {
        $("#new_test_form").attr('action', '/tests/create_new_test/').submit();
    });
    $("#new_test_submit_edit").click(function () {
        $("#new_test_form").attr('action', '/tests/create_new_test_edit/').submit();
    });
    //创建练习时选择加入的班级的列表初始化
    let csrf = $('input[name="csrfmiddlewaretoken"]').val();
    $.ajax({
        url: "/api/find_my_classes/",
        type: "POST",
        data: {"csrfmiddlewaretoken": csrf},
    }).done(function (response) {
        response = eval(response);
        let options = "";
        for (let n in response) {
            options += "<option class='class-select-option' value='" + response[n].id + "'>" + response[n].name + "</option>";
        }
        $("#create_test_classes").append(options);
        $("#create_test_classes").selectpicker('refresh');
    });
    //绑定列表事件
    $("#create_test_classes").change(function () {
        $("input[name=create_test_classes]").val($("#create_test_classes").val())
    });
});

function updateImageDisplay() {
    let img_uploader = document.getElementById('new_test_img_uploader');
    let preview = document.getElementById("new_test_img_upload_btn");
    let img_server = document.getElementById('new_test_img_upload_btn').getAttribute('url');
    let curFile = img_uploader.files[0];
    console.log(curFile);
    let formData = new FormData();
    formData.append('file', curFile);
    $.ajax({
        url: img_server + "upload/",
        type: "POST",
        contentType: false,
        processData: false,
        data: formData,
    }).done(function (response) {
        response = response.replace(/\n/g, '');
        response = response.replace(/ /g, '');
        let regex = /MD5:(.*)(?=<\/h1>)/g;
        let url = img_server + regex.exec(response)[1];
        document.getElementById("new_test_cover").value = url;
    });
    let url = URL.createObjectURL(curFile);
    preview.style.backgroundImage = "url(" + url + ")";
}