$(document).ready( () => {

    let chart1 = document.getElementById('chart1');
    let c_id = chart1.dataset.c_id;
    $.ajax({
        url: "/api/get_chart_data/" + c_id + "/",
        type: "GET",
    }).done(function (response) {
        // 基于准备好的dom，初始化echarts实例
        let myChart1 = echarts.init(document.getElementById('chart1'));
        let myChart2 = echarts.init(document.getElementById('chart2'));
        let data = JSON.parse(response);
        let test_name = data[0]
        // 指定图表的配置项和数据
        let option1 = {
            title: {
                text: '人均练习次数'
            },
            tooltip: {},
            legend: {
                data:['人均练习次数']
            },
            xAxis: {
                type: 'category',
                data: test_name
            },
            yAxis: {type: 'value'},
            series: [{
                name: '人均练习次数',
                type: 'line',
                data: data[2]
            }]
        };
        let angle_data = [];
        let i;
        for(i in data[0])
        {
            let data_dic = {};
            data_dic['name'] = data[0][i];
            data_dic['value'] = data[1][i];
            angle_data[i] = data_dic;
        }
        let option2 = {
            legend: {top: 'bottom'},
            toolbox: {show: true,
                feature: {
                mark: {show: true},
                }
            },
            title:{text: '人均练习时间'},
            series : [
                {
                    name: '人均练习时间',
                    type: 'pie',
                    radius: [50,150],
                    center:['50%','50%'],
                    roseType: 'angle',
                    data:angle_data,
                    itemStyle: {borderRadius: 8},}]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart1.setOption(option1);
        myChart2.setOption(option2);
    });


    $(".student-grade-detail").click(function () {
        let username = this.dataset.username;
        let user_id = this.dataset.student_id;
        let test_id = this.dataset.test_id;
        let class_id = this.dataset.class_id;
        $.ajax({
            url: "/api/student_grade_detail/" + class_id + "/" + test_id + "/" + user_id + "/",
            type: "GET",
        }).done(function (response) {
            response = eval(response);//计算字符串
            console.log(response);//打印结果
            $("#username").text(username);
            $("#test_id").text(test_id);

            // 删掉旧的head和body
            $('#table1').remove();
            $('.modal-body').append('<table id="table1" class="table"></table>');

            let page;
            let head_tr = document.createElement('tr');
            let times_th = document.createElement('th');
            times_th.innerHTML = '次数';
            head_tr.append(times_th);
            let thead = document.createElement('thead');

            for (page = 1; page<=response[0].pages; page++)
            {
                let th = '第'+page+'页';
                let one_th = document.createElement('th');
                one_th.innerHTML = th;
                head_tr.append(one_th);
            }
            thead.append(head_tr);
            $('#table1').append(thead);
            let tbody = document.createElement('tbody');
            let one_record;
            let acount = 1;
            for (one_record in response)
            {
                let body_tr = document.createElement('tr');
                let one_test_time = response[one_record].spend_time;
                let one_page_time;
                let one_time_th = document.createElement('th');
                let count = '第'+acount+'次';
                one_time_th.innerHTML = count;
                body_tr.append(one_time_th);
                for(one_page_time in one_test_time)
                {
                    let time = one_test_time[one_page_time] + 's';
                    let one_th = document.createElement('th');
                    one_th.innerHTML = time;
                    body_tr.append(one_th);
                }
                tbody.append(body_tr);
                acount++;
            }
            $('#table1').append(tbody);
        });
    });
});