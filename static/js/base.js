$(document).ready(() => {
    $(".dropdown").mouseenter(function () {
        $(this).addClass('open');
    }).mouseleave(function () {
        $(this).removeClass('open');
    });
    $('#search_btn_header').click(function () {
        let search = $('#search_input_header').val();
        let popover = $('.search-popover-header').empty();
        if (search.length === 0) {
            window.alert('请输入搜索关键字！');
            return;
        }
        $.ajax({
            url: '/api/search/',
            type: 'GET',
            data: {
                'search': search,
                'range': 'all',
            }
        }).done(function (response, status, xhr) {
            //成功获取到信息
            if (status === 'success' && xhr.status === 200) {
                let input = $('#search_input_header');
                let popover = $('.search-popover-header');
                let result = JSON.parse(response);
                // 判断结果是否为空
                if(result.length === 0){
                    let result_row = $('' +
                        '<div style="vertical-align: center; margin:0 auto">' +
                            '<div style="display: flex; justify-content: center; align-items:center;">' +
                                '<img style="width: 40px; height: 40px;" src="/static/icon/Result_not_found.svg"/>' +
                                '<b>没有找到搜索结果</b>' +
                            '</div>' +
                        '</div>'
                    );
                    popover.append(result_row);
                }

                //显示弹出的结果框，并且绑定点击别处结果框消失事件
                popover.css('display', 'block');
                popover.width(input.width() * 2);
                $(document).bind('click', function (event) {
                    let e = event || window.event;
                    let elem = e.target || e.srcElement;
                    elem = $(elem);
                    while(elem.length > 0){
                        if(elem.attr('class') && elem.attr('class') === 'search-popover-header' || elem.attr('id') === 'search_input_header') {
                            return false;
                        }
                        // 点击到搜索结果内部元素
                        elem = elem.parent();
                    }
                    popover.css('display', 'none');
                });
                //插入单条搜索结果
                for (let n in result) {
                    let iconName, url;
                    switch(result[n].type){
                        case 'class':
                            iconName = 'C_round_solid.svg';
                            url = '/classes/' + result[n].id + '/';
                            break;
                        case 'test':
                            iconName = 'T_round_solid.svg';
                            url = '/tests/' + result[n].id + '/';
                            break;
                        case 'user':
                            iconName = 'U_round_solid.svg';
                            url = '/user/' + result[n].id + '/';
                            break;
                    }
                    let result_row = $(
                        '<div class="row search-result-row-header" data-url="' + url + '">' +
                        '<div class="col-md-2">' +
                        '<img src="/static/icon/' + iconName + '" style="width: 45px; height: 45px;" alt="Class"/>' +
                        '</div>' +
                        '<div class="col-md-8 col-md-offset-1" style="padding-left: 0 !important;">' +
                        '<div class="row"><b>' + result[n].name + '</b></div>' +
                        '<div class="row search-result-row-description-header">@' + result[n].belong + '</div>' +
                        '</div>' +
                        '</div>' +
                        '<hr style="margin-top: 1px; margin-bottom: 1px;"/>'
                    );
                    popover.append(result_row);
                    $('.search-result-row-header').click( function () {
                        window.location.href = $(this).data('url');
                    });
                }
            } else {
                //获取信息失败，显示错误界面
                console.log('error');
            }

        });
    });
    // 需要添加页面缩放时的处理事件，修改元素的大小
});