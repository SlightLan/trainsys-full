from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from api import api_views

urlpatterns = [
    # path('test/'),
    # path('test/page/'),
    path('submit_test/', api_views.submit_test),
    path('find_do_test_info_by_name/', api_views.find_do_test_info_by_name),
    path('find_test_by_name/', api_views.find_test_data_by_name),
    path('find_test_id_by_name/', api_views.find_test_id_by_name),
    path('find_do_test_by_name/', api_views.find_do_test_by_name),
    path('exercise_page_finished/', api_views.add_exercise_record),
    path('find_my_classes/', api_views.find_my_classes),
    url(r'find_test_by_id/(\d+)/', api_views.find_test_by_id),
    url(r'student_grade_detail/(\d+)/(\d+)/(\d+)/', api_views.student_grade_detail),
    url(r'get_chart_data/(\d+)/', api_views.get_chart_data),
    path('send_do_test_result/', api_views.receive_do_test_result),
    path('find_current_class/', api_views.find_current_class),
    path('find_img_server/', api_views.find_img_server),
    path('find_user/', api_views.find_user),
    url(r'search/$', api_views.search),
    path('upload_file/', api_views.upload_file),
    url(r'delete_file/(\d+)/([^\/\\\:\*\"\<\>\|\?]+\.\w+|\w+)/', api_views.delete_file),
]