from django.shortcuts import render, HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from test_op.models import Test
from login.models import User
from class_op.models import Class
from grade.models import Exercise, DetailRecord
from TrainSys.utils import IMG_SERVER
import json
import os
# Create your views here.

# @method_decorator(csrf_exempt)
from TrainSys.global_func import mLogin_required


@mLogin_required
def submit_test(request):
    if request.method == 'POST':
        data = {}
        if request.POST:
            data = request.body.decode('utf-8')
            test_name = request.session.get('test_name')
            test = Test.objects.get(name=test_name)
            test.data = data
            test.save()
        return HttpResponse(request)
    return HttpResponse("fuck")


def find_test_data_by_name(request):
    test_name = request.session.get('test_name')
    test = Test.objects.get(name=test_name)
    return HttpResponse(test.data)


def find_do_test_info_by_name(request):
    test_name = request.session.get('do_test_name')
    test = Test.objects.filter(name=test_name).all().values('name', 'describe', 'teacher')
    test = list(test)
    print(test)
    teacher_name = User.objects.get(id=test[0]['teacher']).display_name
    test[0]['teacher'] = teacher_name
    result = json.dumps(test, ensure_ascii=False)
    return HttpResponse(result)


def find_test_id_by_name(request):
    test_name = request.session.get('test_name')
    test = Test.objects.get(name=test_name)
    return HttpResponse(test.id)


def find_do_test_by_name(request):
    test_name = request.session.get('do_test_name')
    test = Test.objects.get(name=test_name)
    return HttpResponse(test.data)


@mLogin_required
def add_exercise_record(request):
    if request.method == 'POST':
        test_name = request.session.get('do_test_name')
        test = Test.objects.get(name=test_name)
        user_id = request.session.get('user_id')
        user = User.objects.get(id=user_id)
        exercise = Exercise()
        exercise.test = test
        exercise.user = user
        exercise.page = int(request.body)
        exercise.save()
        return HttpResponse("True")
    else:
        return HttpResponse("False")


@mLogin_required
def find_my_classes(request):
    if request.method == 'POST':
        user_id = request.session.get('user_id')
        user = User.objects.get(id=user_id)
        classes = user.own_classes_set.all().order_by('-id').values('id', 'name')
        result = json.dumps(list(classes), ensure_ascii=False)

        return HttpResponse(result)
    else:
        return HttpResponse([])


def find_test_by_id(request, data):
    test = Test.objects.filter(id=data).all().values('id', 'name', 'cover', 'describe', 'teacher')
    test = list(test)
    test_data = Test.objects.get(id=data).data
    if test_data == '':
        test[0]['data'] = False
    else:
        test[0]['data'] = True
    teacher_name = User.objects.get(id=test[0]['teacher']).display_name
    test[0]['teacher'] = teacher_name
    result = json.dumps(test, ensure_ascii=False)
    return HttpResponse(result)


@mLogin_required
def student_grade_detail(request, class_id, test_id, user_id):
    print('____________________')
    test_student_records = DetailRecord.objects.filter(_class=class_id).filter(user=user_id).filter(test=test_id)
    test_student_records = list(test_student_records)
    result = []
    times = 0
    for one_time in test_student_records:
        record = {}
        times += 1
        record['times'] = times
        spend_click = []
        spend_time = []
        time_string_list = one_time.spend_time[1:-1].split(',')
        for time_string in time_string_list:
            spend_time.append(int(time_string))
        click_string_list = one_time.spend_click[1:-1].split(',')
        for click_string in click_string_list:
            spend_click.append(int(click_string))
        record['spend_click'] = spend_click
        record['spend_time'] = spend_time
        record['pages'] = len(spend_time)
        result.append(record)
    result = json.dumps(result, ensure_ascii=False)
    print(result)
    return HttpResponse(result)


#获取统计图表所需要的的数据
def get_chart_data(request,class_id):
    student_records = DetailRecord.objects.filter(_class=class_id)
    tests = Test.objects.filter(classes=class_id)
    c = Class.objects.get(id=class_id)
    students_number = float(len(c.students.all()))
    chart1 = {}
    test_name = []
    for test in tests:
        test_name.append(test.name)
        chart1[test.name] = [0, 0]
        for record in student_records:
            if record.test_id == test.id:
                time_string_list = record.spend_time[1:-1].split(',')
                for time_string in time_string_list:
                    chart1[test.name][0] += int(time_string)
                chart1[test.name][1] += 1
        if chart1[test.name][0] != 0:
            chart1[test.name][0] = chart1[test.name][0]/chart1[test.name][1]#平均每做一次需要花的时间
        if chart1[test.name][1] != 0:
            chart1[test.name][1] = float(chart1[test.name][1])/students_number#人均做了多少次
    print(chart1)
    result = []
    result.append(test_name)
    average_spend_time = []
    average_do_times = []
    for a in test_name:
        average_spend_time.append(round(chart1[a][0],2))
        average_do_times.append(round(chart1[a][1],2))
    result.append(average_spend_time)
    result.append(average_do_times)
    print(result)
    return HttpResponse(json.dumps(result))


@mLogin_required
def receive_do_test_result(request):
    if request.method == 'POST':
        data = str(request.body, encoding="utf-8")
        data = json.loads(data)
        test_name = data['test_name']
        spend_time = data['spend_time']
        spend_click = data['spend_click']
        u = User.objects.get(id=request.session.get('user_id'))
        t = Test.objects.get(name=test_name)
        c = Class.objects.get(id=request.session['do_test_class'])
        detailRecord = DetailRecord()
        detailRecord.test = t
        detailRecord.user = u
        detailRecord._class = c
        detailRecord.spend_time = spend_time
        detailRecord.spend_click = spend_click
        detailRecord.save()
        return HttpResponse("OK")
    else:
        return HttpResponse("ERROR")


@mLogin_required
def find_current_class(request):
    return HttpResponse(request.session.get('current_class'))


def find_img_server(request):
    return HttpResponse(IMG_SERVER)


def find_user(request):
    user_id = request.session.get('user_id')
    user = User.objects.get(id=user_id)
    user_data = {
        'name': user.display_name
    }
    user_data = json.dumps(user_data)
    return HttpResponse(user_data)


@mLogin_required
def search(request):
    search_string = request.GET['search']
    search_range = request.GET['range']
    result = []
    if search_range == 'all' or search_range == 'class':
        class_search = Class.objects.filter(name__contains=search_string)
        for c in class_search:
            result.append({
                'id': c.id,
                'name': c.name,
                'belong': list(c.teachers.all())[0].display_name,
                'cover': c.cover,
                'type': 'class',
            })
    if search_range == 'all' or search_range == 'test':
        test_search = Test.objects.filter(name__contains=search_string)
        for t in test_search:
            result.append({
                'id': t.id,
                'name': t.name,
                'belong': t.teacher.display_name,
                'cover': t.cover,
                'type': 'test',
            })
    if search_range == 'all' or search_range == 'user':
        user_search = User.objects.filter(display_name__contains=search_string)
        for u in user_search:
            result.append({
                'id': u.id,
                'name': u.display_name,
                # 未来区分用户名和昵称的时候可用
                'belong': u.username,
                'identity': u.identity,
                'type': 'user',
            })

    result = json.dumps(result, ensure_ascii=False)

    return HttpResponse(result)


@mLogin_required
def upload_file(request):
    if request.method == 'POST':
        file = request.FILES.get('upload-files')
        class_id = request.POST.get('class_id')
        _class = Class.objects.get(id=class_id)
        class_files_list = _class.files.split(',')
        if file.name in class_files_list:
            return HttpResponse('已存在同名文件')

        path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'static', 'uploads')
        if os.path.exists(os.path.join(path, class_id, file.name)):
            return HttpResponse('文件已存在')
        else:
            try:
                os.mkdir(path + '/' + class_id)
            except Exception as error:
                if not error.args[0] == 17:
                    return HttpResponse('创建文件失败')
            path = os.path.join(path, class_id, file.name)
            try:
                f = open(path, 'wb')
                for bite in file:
                    f.write(bite)
                f.close()
            except Exception as error:
                return HttpResponse(error)
            else:
                _class.files = _class.files + ',' + file.name
                _class.save()
    return HttpResponse('200')


@mLogin_required
def delete_file(request, class_id, file):
    path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'static', 'uploads')
    if os.path.exists(os.path.join(path, class_id, file)):
        try:
            os.remove(os.path.join(path, class_id, file))
        except Exception as error:
            return HttpResponse(error)
        else:
            _class = Class.objects.get(id=class_id)
            file_in_db = ',' + file
            _class.files = _class.files.replace(file_in_db, '')
            _class.save()
            return HttpResponse(200)
    else:
        return HttpResponse('File not exists')
