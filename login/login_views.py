from django.shortcuts import render, redirect, HttpResponse
from django.contrib import messages
from django import forms
from . import models
import re
import hashlib

# t1ach2r
Teacher_Verify_Code = 'c1a20c9082f0f576adfd1f06d21077772b359231666e45209267be10b3bd0d76'


class VerifyCodeForm(forms.Form):
    teacher_verify_code = forms.CharField(max_length=7, min_length=7)


def hash_string(string, salt='TrainSys'):
    h = hashlib.sha256()
    string += salt
    h.update(string.encode())
    return h.hexdigest()


def if_illegal(request, string, length, if_upper):
    # check length
    if length[0] <= len(string) <= length[1]:
        # check illegal char
        if re.search(u'^[_a-zA-Z0-9]+$', string):
            if if_upper:
                if re.search(u'[A-Z]+', string):
                    return True
                else:
                    messages.add_message(request, messages.INFO, '密码首字母应为大写')
                    return False
            else:
                return True
        messages.add_message(request, messages.INFO, '用户名或密码包含非法字符')
        return False
    messages.add_message(request, messages.INFO, '用户名或密码长度错误')
    return False


# Create your views here.


def login(request):
    if request.session.get('is_login'):
        return redirect('/classes/')
    if request.method == "POST":
        username = request.POST.get('username').strip()
        password = request.POST.get('password').strip()
        # verify
        if if_illegal(request, username,[4, 16], False) and if_illegal(request, password, [6, 16], True):
            try:
                user = models.User.objects.get(username=username)
            except:
                messages.add_message(request, messages.INFO, '用户名或密码错误')
                return render(request, 'login/login.html')
            if user.password == hash_string(password):
                # 实际上并不能检测异地已登录问题，需要完善机制
                if request.session.get('is_login'):
                    messages.add_message(request, messages.INFO, '用户已登录')
                    return render(request, 'login/login.html')
                request.session['is_login'] = True
                request.session['user_id'] = user.id
                request.session['username'] = user.username
                request.session['display_name'] = user.display_name
                request.session['identity'] = user.identity
                prev_url = request.COOKIES.get('prev_url', '')
                if prev_url:
                    return redirect(prev_url)
                else:
                    return redirect('/classes/')
            else:
                request.session['username'] = username
                messages.add_message(request, messages.INFO, '用户名或密码错误')
                return render(request, 'login/login.html')
        else:
            return render(request, 'login/login.html')
    else:
        if request.session.get('is_login'):
            return render(request, 'login/login.html')
    return render(request, 'login/login.html')


def register(request):
    if request.session.get('is_login'):
        messages.add_message(request, messages.INFO, '你已经注册了，请勿重复注册')
    if request.method == "POST":
        username = request.POST.get('username').strip()
        display_name = request.POST.get('display_name').strip()
        password1 = request.POST.get('password1').strip()
        password2 = request.POST.get('password2').strip()
        if if_illegal(request, username, [4,16], False) and if_illegal(request, password1, [6, 16], True)\
                and if_illegal(request, password2, [6, 16], True):
            if password1 != password2:
                messages.add_message(request, messages.INFO, '两次密码输入应保持一致')
                return render(request, 'login/register.html')
            if models.User.objects.filter(username=username):
                messages.add_message(request, messages.INFO, '用户名已存在')
                return render(request, 'login/register.html')
            else:
                new_user = models.User()
                new_user.username = username
                new_user.display_name = display_name
                new_user.password = hash_string(password1)
                new_user.identity = 'student'
                if request.session.get('teacher_verify_code'):
                    if request.session.get('teacher_verify_code') == Teacher_Verify_Code:
                        new_user.identity = 'teacher'
                        request.session['identity'] = 'teacher'
                new_user.save()
                new_user = models.User.objects.get(username=username)
                request.session['is_login'] = True
                request.session['user_id'] = new_user.id
                request.session['display_name'] = display_name
                return redirect('/classes/')
    return render(request, 'login/register.html')


def logout(request):
    if not request.session.get('is_login'):
        return redirect('/login/')
    request.session.flush()
    return redirect("/login/")


def teacher_verify(request):
    if request.method == 'POST':
        # 需要限制输入的格式和长度，防止溢出攻击
        form_obj = VerifyCodeForm(request.POST)
        status = form_obj.is_valid()
        if status:
            code = hash_string(request.POST.get('teacher_verify_code'))
            if code == Teacher_Verify_Code:
                request.session['teacher_verify_code'] = code
                return HttpResponse('done')
    return HttpResponse('error')
