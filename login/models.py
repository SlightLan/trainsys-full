from django.db import models

# Create your models here.


class User(models.Model):
    username = models.CharField(max_length=32, unique=True)
    display_name = models.CharField(max_length=6, unique=False)
    password = models.CharField(max_length=64)
    create_time = models.DateField(auto_now_add=True)
    identity = models.CharField(max_length=8, choices=(('teacher', '教师'), ('student', '学生')), default='student')

    class Mete:
        ordering = ['create_time']
