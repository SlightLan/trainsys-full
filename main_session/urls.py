from django.urls import path
from django.views.generic import TemplateView

urlpatterns = [
    path('', TemplateView.as_view(template_name="index.html")),
    path('create_test/', TemplateView.as_view(template_name="create_test.html")),
    path('do_test/', TemplateView.as_view(template_name="do_test.html")),
]