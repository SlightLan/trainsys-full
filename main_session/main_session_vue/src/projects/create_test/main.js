import Vue from 'vue'
import MainPage from './views/MainPage.vue'
import store from './store'
import router from './router'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

Vue.config.productionTip = false



new Vue({
  store,
  router,
  render: h => h(MainPage)
}).$mount('#app')
