class AdjustElement{
  constructor(dragMinWidth, dragMinHeight, X, Y, width, height, asideMargin, headerMargin, id){
    this.dragMinWidth = dragMinWidth;
    this.dragMinHeight = dragMinHeight;
    this.initX = X;
    this.initY = Y;
    this.initWidth = (width > dragMinWidth ? width : dragMinWidth);
    this.initHeight = (height > dragMinHeight ? height : dragMinHeight);
    this.asideMargin = asideMargin;
    this.headerMargin = headerMargin;
    this.id = id;
    this.windowWidth = document.documentElement.clientWidth;
    this.windowHeight = document.documentElement.clientHeight;
  }
  initSize(){
    let oDrag = document.getElementById(this.id);
    oDrag.style.left = this.initX + "px";
    oDrag.style.top = this.initY + "px";
    oDrag.style.width = this.initWidth + "px";
    oDrag.style.height = this.initHeight + "px";
  }
  bindResize(){
    let oDrag = document.getElementById(this.id);
    let oTitle = this.getByClass("title", oDrag)[0];
    let oL = this.getByClass("resizeL", oDrag)[0];
    let oT = this.getByClass("resizeT", oDrag)[0];
    let oR = this.getByClass("resizeR", oDrag)[0];
    let oB = this.getByClass("resizeB", oDrag)[0];
    let oLT = this.getByClass("resizeLT", oDrag)[0];
    let oTR = this.getByClass("resizeTR", oDrag)[0];
    let oBR = this.getByClass("resizeBR", oDrag)[0];
    let oLB = this.getByClass("resizeLB", oDrag)[0];
    this.drag(oDrag, oTitle);
    //四角
    this.Resize(oDrag, oLT, true, true, false, false);
    this.Resize(oDrag, oTR, false, true, false, false);
    this.Resize(oDrag, oBR, false, false, false, false);
    this.Resize(oDrag, oLB, true, false, false, false);
    //四边
    this.Resize(oDrag, oL, true, false, false, true);
    this.Resize(oDrag, oT, false, true, true, false);
    this.Resize(oDrag, oR, false, false, false, true);
    this.Resize(oDrag, oB, false, false, true, false);
    //oDrag.style.left = (document.documentElement.clientWidth - oDrag.offsetWidth) / 2 + "px";
    //oDrag.style.top = (document.documentElement.clientHeight - oDrag.offsetHeight) / 2 + "px";
  }
  onWindowResize(){
    this.windowWidth = document.documentElement.clientWidth;
    this.windowHeight = document.documentElement.clientHeight;
    this.bindResize();
  }
  getById(id){
    return typeof id === "string" ? document.getElementById(id) : id;
  }
  getByClass(_class, _parent){
    let aClass = [];
    let reClass = new RegExp("(^| )" + _class + "( |$)");
    let aElement = this.getByTagName("*", _parent);
    for(let i = 0; i < aElement.length; i++){
      reClass.test(aElement[i].className) && aClass.push(aElement[i]);
    }
    return aClass;
  }
  getByTagName(elem, obj){
    return (obj || document).getElementsByTagName(elem);
  }
  initial(isResizeable=true){
    if(isResizeable){
      this.bindResize()
    }else{
      this.initWidth = "25";
      this.initHeight = "25";
      let oDrag = document.getElementById(this.id);
      let count;
      // 删除所有resize组件
      for(count = 0; count < 8; count ++) {
        oDrag.removeChild(oDrag.childNodes[1]);
      }
    }
    this.initSize();
  }
  drag(oDrag, handle){ //oDrag:被拖动对象
    let _this = this;
    let X = 0, Y = 0;
    // let oMin = this.getByClass("min", oDrag)[0];
    // let oMax = this.getByClass("max", oDrag)[0];
    // let oRevert = this.getByClass("revert", oDrag)[0];
    // let oClose = this.getByClass("close", oDrag)[0];
    handle = handle || oDrag;
    handle.style.cursor = "move" //定义光标类型
    handle.onmousedown = function(event){
      event = event || window.event;
      X = event.clientX - oDrag.offsetLeft;
      Y = event.clientY - oDrag.offsetTop;
      document.onmousemove = function(event){
        event = event || window.event;
        let iL = event.clientX - X;
        let iT = event.clientY - Y;
        let maxL = _this.windowWidth - oDrag.offsetWidth;
        let maxT = _this.windowHeight - oDrag.offsetHeight;
        iL <= _this.asideMargin && (iL = _this.asideMargin);
        iT <= _this.headerMargin && (iT = _this.headerMargin);
        iL >= maxL && (iL = maxL);
        iT >= maxT && (iT = maxT);
        oDrag.style.left = iL + "px";
        oDrag.style.top = iT + "px";
        event.stopPropagation();
        return false;
      };
      document.onmouseup = function(){
        document.onmousemove = null;
        document.onmouseup = null;
      };
      event.stopPropagation();
      return false;
    };
    // oMax.onclick = function(){
    //   oDrag.style.top = oDrag.style.left = "0px";
    //   oDrag.style.width = _this.windowWidth - 2 + "px";
    //   oDrag.style.height = _this.windowHeight - 2 + "px";
    //   this.style.display = "none";
    //   oRevert.style.display = "block";
    // };
    // oRevert.onclick = function (){
    //   oDrag.style.width = _this.dragMinWidth + "px";
    //   oDrag.style.height = _this.dragMinHeight + "px";
    //   oDrag.style.left = (_this.dragMinWidth - oDrag.offsetWidth) / 2 + "px";
    //   oDrag.style.top = (_this.windowHeight - oDrag.offsetHeight) / 2 + "px";
    //   this.style.display = "none";
    //   oMax.style.display = "block";
    // };
    // oMin.onclick = oClose.onclick = function(){ //最小化和关闭都是用了让其最小的处理流程
    //   oDrag.style.display = "none";
    //   let oA = document.createElement("a");
    //   oA.className = "open";
    //   oA.href = "javascript:;";
    //   oA.title = "还原";
    //   document.body.appendChild(oA);
    //   oA.onclick = function (){
    //     oDrag.style.display = "block";
    //     document.body.removeChild(this);
    //     this.onclick = null;
    //   };
    // };
    // oMin.onmousedown = oMax.onmousedown = oClose.onmousedown = function (event){ //防止事件上传到父节点，阻断事件传播
    //   this.onfocus = function () {this.blur()};
    //   (event || window.event).cancelBubble = true
    // };
  }
  Resize(oParent, handle, isLeft, isTop, lockX, lockY) {
    let _this = this;
    handle.onmousedown = function (event) {
      event = event || window.event;
      let disX = event.clientX - handle.offsetLeft;
      let disY = event.clientY - handle.offsetTop;
      let iParentTop = oParent.offsetTop;
      let iParentLeft = oParent.offsetLeft;
      let iParentWidth = oParent.offsetWidth;
      let iParentHeight = oParent.offsetHeight;
      document.onmousemove = function (event) {
        event = event || window.event;
        let iL = event.clientX - disX;
        let iT = event.clientY - disY;
        let maxW = _this.windowWidth - oParent.offsetLeft - 2;
        let maxH = _this.windowHeight - oParent.offsetTop - 2;
        let iW = isLeft ? iParentWidth - iL : handle.offsetWidth + iL;
        let iH = isTop ? iParentHeight - iT : handle.offsetHeight + iT;
        isLeft && (oParent.style.left = iParentLeft + iL + "px");
        isTop && (oParent.style.top = iParentTop + iT + "px");
        iW < _this.dragMinWidth && (iW = _this.dragMinWidth);
        iW > maxW && (iW = maxW);
        lockX || (oParent.style.width = iW + "px");
        iH < _this.dragMinHeight && (iH = _this.dragMinHeight);
        iH > maxH && (iH = maxH);
        lockY || (oParent.style.height = iH + "px");
        if ((isLeft && iW === _this.dragMinWidth) || (isTop && iH === _this.dragMinHeight)) document.onmousemove = null;
        event.stopPropagation();
        return false;
      };
      event.stopPropagation();
      document.onmouseup = function () {
        document.onmousemove = null;
        document.onmouseup = null;
      };
      return false;
    };
  }
}

export default AdjustElement;