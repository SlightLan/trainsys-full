import Vue from 'vue'
import VueRouter from 'vue-router'
import MateElement from '../views/MateElement.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/create_test/',
    name: 'MateElement',
    component: MateElement
  },
  {
    path: '/create_test/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // 就是说创建一个共同的about.js文件
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
