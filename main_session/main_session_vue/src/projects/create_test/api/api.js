import {get, post, getTest} from './axios'

export function postData(url, data){
  post(url, data);
}

export function getData(url){
    return get(url);
}

export function getTestData(url){
    return getTest(url)
}