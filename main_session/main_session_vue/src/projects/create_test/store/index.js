import Vue from 'vue'
import Vuex from 'vuex'
import {elementAttributes} from './utils.js'

Vue.use(Vuex)

function copyElementData(elementData){
  let newElementData = [];
  for(let n in elementData){
    let newData = {};
    newData["id"] = elementData[n].id;
    newData["zIndex"] = elementData[n].zIndex;
    newData["attributes"] = {};
    for(let attr in elementData[n].attributes){
      newData["attributes"][attr] = elementData[n].attributes[attr];
    }
    newElementData.push(newData);
  }
  return newElementData;
}


export default new Vuex.Store({
  state: {
    mainPageAsideWidth:'',
    mainPageHeaderHeight:'',
    elementData: [{id: 0, zindex: 0, attributes: {width: "", height: "", bgUrl: "", turning: ""} }],
    dialogSwitch: 0,
    deleteSwitch: 0,
    pageData: [],
    currentPage: 0,
    maxPage: 0,
    imgServer: '',
    imgServer_upload: '',
  },
  mutations: {
    setAsideMargin(state, data){
      state.mainPageAsideWidth = data;
    },
    setHeaderMargin(state, data){
      state.mainPageHeaderHeight = data;
    },
    addElementData(state, data){
      state.elementData.push(data);
    },
    setElementData(state, data){
      for(let key in data.data){
        state.elementData[data.id]["attributes"][key] = data.data[key];
      }
    },
    addPageData(state){
    //save old page
      let oldElementData = copyElementData(state.elementData);
      let container = document.getElementById("adjustElementContainer");
      state.pageData[state.currentPage] = oldElementData;
    //open new page
      state.pageData.splice(state.currentPage + 1, 0, [{}]);
      state.elementData = [{id: 0, attributes: {
        width: container.clientWidth,
        height: container.clientHeight,
        bgUrl: "",
        turning: ""} }];
      state.currentPage += 1;
    },
    setPageData(state, data){
      state.pageData = data;
      state.elementData = state.pageData[state.currentPage];
    },
    savePageData(state){
      let oldElementData = copyElementData(state.elementData);
      state.pageData[state.currentPage] = oldElementData;
    },
    changePageData(state, data){
      let oldElementData = copyElementData(state.elementData);
      state.pageData[state.currentPage] = oldElementData;
      state.elementData = [];
      state.elementData = state.pageData[data];
      state.currentPage = data;
    },
    openDialog(state, data){
      state.dialogSwitch = data;
    },
    closeDialog(state, data){
      state.dialogSwitch = 0;
    },
    setDelete(state, data){
      state.deleteSwitch = data;
    },
    setImgServer(state, data) {
      state.imgServer = data;
      state.imgServer_upload = data + 'upload';
    },
  }
  ,
  getters: {
    getAsideMargin: state => {
      return state.mainPageAsideWidth;
    },
    getHeaderMargin: state => {
      return state.mainPageHeaderHeight;
    },
    getElementsData: state => {
      return state.elementData;
    },
    getSingleElementData: state => (id) => {
      for(let n in state.elementData){
        if(state.elementData[n]["id"] == id){
          return state.elementData[n];
        }
      }
    },
    getDialog: state => {
      return state.dialogSwitch;
    },
    getDelete: state => {
      return state.deleteSwitch;
    },
    getPageData: state => {
      return state.pageData;
    },
    getCurrentPage: state => {
      return state.currentPage;
    },
    getImgServer: state => {
      return state.imgServer;
    },
    getImgServerUpload: state => {
      return state.imgServer_upload;
    },
  },
  actions: {
    setAsideMargin({commit}, data){
      commit('setAsideMargin', data);
    },
    setHeaderMargin({commit}, data){
      commit('setHeaderMargin', data);
    },
    addPageData({commit}){
      commit('addPageData');
    },
    setPageData({commit}, data){
      commit('setPageData', data);
    },
    addElementData({commit}, data){
      let attributes = {};
      for(let attr in elementAttributes){
        attributes[attr] = elementAttributes[attr];
      }
      for(let attr in data.attributes){
        attributes[attr] = data.attributes[attr];
      }
      let Data = {
        id: data.id || -1,
        zIndex: 0,
        attributes: attributes,
      };
      commit('addElementData', Data);
    },
    setElementData({commit}, data){
      commit('setElementData', data);
    },
    openDialog({commit}, data){
      commit('openDialog', data);
    },
    closeDialog({commit}, data){
      commit('closeDialog', data);
    },
    setDelete({commit}, data){
      commit('setDelete', data);
    },
    changePageData({commit}, data){
      commit('changePageData', data);
    },
    savePageData({commit}, data){
      commit('savePageData', data);
    },
    setImgServer({commit}, data){
      commit('setImgServer', data);
    }
  },
  modules: {
  }
})
