export const elementAttributes = {
         name: '',
         type: 'empty',
         text: '',
         parameters: '',
         bgUrl: '',

         toSwitch: '',
         bySwitch: '',
         toState: '',
         byState: '',

         width: 0,
         height: 0,
         left: 0,
         top: 0,

         bold: '0',
         underline: '0',
         alignLeft: '1',
         alignCenter: '0',
         size: '14',
};