import Vue from 'vue'
import Vuex from 'vuex'
import {elementAttributes} from './utils.js'

Vue.use(Vuex)

function copyElementData(elementData){
  let newElementData = [];
  for(let n in elementData){
    let newData = {};
    newData["id"] = elementData[n].id;
    newData["zIndex"] = elementData[n].zIndex;
    newData["attributes"] = {};
    for(let attr in elementData[n].attributes){
      newData["attributes"][attr] = elementData[n].attributes[attr];
    }
    newElementData.push(newData);
  }
  return newElementData;
}


function changeLogicData(state, id, data){
  //保险起见进行转换
  if(data === true){
    data = "1";
  }else if(data === false){
    data = "0";
  }
  for(let n in state.logicData){
    if(state.logicData[n]["id"] === id){
      state.logicData[n]["parameters"] = data;
    }
  }
}


export default new Vuex.Store({
  state: {
    mainPageAsideWidth:'',
    mainPageHeaderHeight:'',
    elementData: [{id: 0, zindex: 0, attributes: {width: "", height: "", bgUrl: "", turning: ""} }],
    dialogSwitch: 0,
    deleteSwitch: 0,
    pageData: [],
    currentPage: 0,
    maxPage: 0,
    logicData: [],
    logicDataChangeFlag: false,
    exerciseResult: [],
    clickCount: 0,
    clickCountEnable: true,
  },
  mutations: {
    setAsideMargin(state, data){
      state.mainPageAsideWidth = data;
    },
    setHeaderMargin(state, data){
      state.mainPageHeaderHeight = data;
    },
    addElementData(state, data){
      state.elementData.push(data);
    },
    setElementData(state, data){
      for(let key in data.data){
        state.elementData[data.id]["attributes"][key] = data.data[key];
      }
    },
    addPageData(state){
    //save old page
      let oldElementData = copyElementData(state.elementData);
      let container = document.getElementById("adjustElementContainer");
      state.pageData[state.currentPage] = oldElementData;
    //open new page
      state.pageData.splice(state.currentPage + 1, 0, [{}]);
      state.elementData = [{id: 0, attributes: {
        width: container.clientWidth,
        height: container.clientHeight,
        bgUrl: "",
        turning: ""} }];
      state.currentPage += 1;
    },
    addLogicData(state, data){
      state.logicData.push(data);
    },
    setPageData(state, data){
      state.pageData = data;
      state.elementData = state.pageData[state.currentPage];
    },
    savePageData(state){
      let oldElementData = copyElementData(state.elementData);
      state.pageData[state.currentPage] = oldElementData;
    },
    changePageData(state, data){
      let oldElementData = copyElementData(state.elementData);
      state.pageData[state.currentPage] = oldElementData;
      state.elementData = [];
      state.elementData = state.pageData[data];
      state.currentPage = data;
    },
    openDialog(state, data){
      state.dialogSwitch = data;
    },
    closeDialog(state, data){
      state.dialogSwitch = 0;
    },
    setDelete(state, data){
      state.deleteSwitch = data;
    },
    triggerSwitch(state, data){
      for(let n in state.elementData){
        if(state.elementData[n]["id"] === data){
          let toSwitch = state.elementData[n].attributes.toSwitch.split('-');
          if(toSwitch == ""){
            return;
          }
          for(let nn in toSwitch){
            let switchData = toSwitch[nn].split(".");
            state.elementData[switchData[0]].attributes.parameters = switchData[1];
            changeLogicData(state, parseInt(switchData[0]), switchData[1]);
          }
          console.log(state.logicData);
        }
      }
      state.logicDataChangeFlag = !state.logicDataChangeFlag;
    },
    addClickCount(state){
      if(state.clickCountEnable){
        state.clickCount += 1;
      }
    },
    resetClickCount(state){
      state.clickCount = 0;
    },
    stopClickCount(state){
      state.clickCountEnable = false;
    }
  },
  getters: {
    getAsideMargin: state => {
      return state.mainPageAsideWidth;
    },
    getHeaderMargin: state => {
      return state.mainPageHeaderHeight;
    },
    getElementsData: state => {
      return state.elementData;
    },
    getSingleElementData: state => (id) => {
      for(let n in state.elementData){
        if(state.elementData[n]["id"] === id){
          return state.elementData[n];
        }
      }
    },
    getLogicDataChangeFlag: state => {
      return state.logicDataChangeFlag;
    },
    getDialog: state => {
      return state.dialogSwitch;
    },
    getDelete: state => {
      return state.deleteSwitch;
    },
    getPageData: state => {
      return state.pageData;
    },
    getCurrentPage: state => {
      return state.currentPage;
    },
    checkSwitch: state => (id) => {
      for(let n in state.elementData){
        if(state.elementData[n]["id"] == id){
          if(id == 0){
            //这是检测rootElement的下一页跳转条件
            let bySwitch = state.elementData[0].attributes.turning.split('.');
            if(bySwitch == ""){
              return true;
            }
            if(state.elementData[parseInt(bySwitch[0])].attributes.parameters == bySwitch[1]){
              return true;
            }else{
              return false;
            }
          }
          if(state.elementData[n].attributes.bySwitch.length === 0){
            return true;
          }
          let bySwitch = state.elementData[n].attributes.bySwitch.split('-');
          for(let n in bySwitch){
            let switchData = bySwitch[n].split('.');
            if(state.elementData[parseInt(switchData[0])].attributes.parameters != switchData[1]){
              return false;
            }
          }
          //for(let nn in {

            //let parameters = state.elementData[bySwitch[nn]].attributes.parameters;
            //if(parameters === '0'){
            //  return false;
           // }
          //}
          return true;
        }
      }
    },
    checkInput:state => (data) => {
      let id = data.id;
      let text = data.text;
      if(text === state.elementData[id].attributes.text){
        return true;
      }else{
        return false;
      }
    },
    getClickCount: state => {
      return state.clickCount;
    }
  },
  actions: {
    setAsideMargin({commit}, data){
      commit('setAsideMargin', data);
    },
    setHeaderMargin({commit}, data){
      commit('setHeaderMargin', data);
    },
    addPageData({commit}){
      commit('addPageData');
    },
    addLogicData({commit}, data){
      commit('addLogicData', data);
    },
    setPageData({commit}, data){
      commit('setPageData', data);
    },
    addElementData({commit}, data){
      let attributes = {};
      for(let attr in elementAttributes){
        attributes[attr] = elementAttributes[attr];
      }
      for(let attr in data.attributes){
        attributes[attr] = data.attributes[attr];
      }
      let Data = {
        id: data.id || -1,
        zIndex: 0,
        attributes: attributes,
      };
      commit('addElementData', Data);
    },
    setElementData({commit}, data){
      commit('setElementData', data);
    },
    openDialog({commit}, data){
      commit('openDialog', data);
    },
    closeDialog({commit}, data){
      commit('closeDialog', data);
    },
    setDelete({commit}, data){
      commit('setDelete', data);
    },
    changePageData({commit}, data){
      commit('changePageData', data);
    },
    savePageData({commit}, data){
      commit('savePageData', data);
    },
    triggerSwitch({commit}, data){
      commit('triggerSwitch', data);
    },
    resetClickCount({commit}){
      commit('resetClickCount');
    },
    addClickCount({commit}){
      commit('addClickCount');
    },
    stopClickCount({commit}){
      commit('stopClickCount');
    }
  },
  modules: {
  }
})