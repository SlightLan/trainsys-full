const path = require('path');
function resolvePath(dir){
    return path.join(__dirname, dir)
}


module.exports = {
    devServer: {
        port: 8001,
    },
    pages: {
    index: 'src/projects/index/main.js',
    create_test: 'src/projects/create_test/main.js',
    do_test: 'src/projects/do_test/main.js',
    },
    chainWebpack:(config)=>{
        config.resolve.alias
        .set('@',resolvePath('./src'))
        .set('assets',resolvePath('./src/assets'))
        .set('components',resolvePath('./src/components'))
        //set第一个参数：设置的别名，第二个参数：设置的路径
    },
    assetsDir: 'static/',
}

