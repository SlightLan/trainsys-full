from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib import messages
from . import models
from login.models import User
import re
import hashlib


def index(request):
    if not request.session.get('is_login'):
        messages.add_message(request, messages.INFO, 'You have not logged in')
        return redirect('/login/')
    if request.method == "GET":
        data = {}
        user_id = request.session.get('user_id')
        user = User.objects.get(id=user_id)
        data['username'] = user.username
        data['password'] = user.password
        data['create_time'] = user.create_time
        return render(request, 'person_info/index.html', data)
    return render(request, 'person_info/index.html')
