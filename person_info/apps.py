from django.apps import AppConfig


class PersonInfoConfig(AppConfig):
    name = 'person_info'
