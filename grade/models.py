from django.db import models
from test_op.models import Test
from login.models import User
from class_op.models import Class

# 用户完成一个页面的任务后会写入一条数据库记录
class Exercise(models.Model):
    test = models.ForeignKey(Test, on_delete=models.CASCADE, related_name="own_exercise_set")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="own_exercise_set")
    time = models.DateTimeField(auto_now_add=True)
    page = models.IntegerField()


# 记录用户做某个练习时花费的时间和点击次数
class DetailRecord(models.Model):
    test = models.ForeignKey(Test, on_delete=models.CASCADE, related_name="own_detailRecord_set")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="own_detailRecord_set")
    _class = models.ForeignKey(Class, on_delete=models.DO_NOTHING, related_name="own_detailRecord_set")
    spend_time = models.CharField(max_length=512)
    spend_click = models.CharField(max_length=512)


#class Exam(models.Model):

