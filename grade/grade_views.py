from django.shortcuts import render, redirect, reverse, HttpResponse
from login.models import User
from django.shortcuts import render, redirect, reverse, HttpResponse
from django.contrib import messages
from . import models
from login.models import User
from class_op.models import Class
from test_op.models import Test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json


# Create your views here.


def grade_classes_list(request):
    if request.method == "GET":
        page = request.GET.get('page')
        data = {}
        classes = Class.objects.all().order_by('-id')
        all_classes = []
        set2 = []
        count = 0
        for c in classes:
            class_ob = {
                'id': c.id,
                'name': c.name,
                'teachers': c.teachers,
                'students_number': len(c.students.all()),
                'cover': (c.cover + '?w=100&h=80'),
            }
            set2.append(class_ob)
            if count < 1:
                count += 1
            else:
                all_classes.append(set2)
                set2 = []
                count = 0
        if count != 0:
            all_classes.append(set2)
        paginator = Paginator(all_classes, 16)
        if page:
            try:
                page = int(page)
                page_data = paginator.page(page)
            except PageNotAnInteger:
                page_data = paginator.page(1)
            except EmptyPage:
                page_data = paginator.page(paginator.num_pages)
            data['page_data'] = page_data
        else:
            page_data = paginator.page(1)
            data['page_data'] = page_data
        return render(request, 'grade/index.html', data)
    return render(request, 'grade/index.html')


# 学生成绩页面参加的课程的列表
def stu_classes_list(request, user_id):
    data = {}
    join_classes = Class.objects.filter(students=user_id)
    join_classes = list(join_classes)
    join_classes_list = []
    count = 0
    set2 = []
    for one_class in join_classes:
        teachers = one_class.teachers.all()
        teachers = list(teachers)
        teacher_name = teachers[0].username
        if 1 < len(teachers):
            for i in range(1, len(teachers)):
                teacher_name = teacher_name + '、' + teachers[i].username
        class_ob = {
            'id': one_class.id,
            'name': one_class.name,
            'describe': one_class.describe,
            'create_time': one_class.create_time,
            'teachers': teacher_name,
            'students_number': len(one_class.students.all()),
            'cover': (one_class.cover + '?w=125&h=110'),
        }
        set2.append(class_ob)
        if count < 1:
            count += 1
        else:
            join_classes_list.append(set2)
            count = 0
            set2 = []
    if count != 0:
        join_classes_list.append(set2)
    data['join_classes'] = join_classes_list
    data['user_id'] = user_id
    return render(request, 'grade/index.html', data)


# 学生页面单个课程成绩
def stu_one_class_grade(request, class_id, user_id):
    data = {}
    stu_class_record = models.DetailRecord.objects.filter(_class=class_id).filter(user=user_id)
    stu_class_record = list(stu_class_record)
    c = models.Class.objects.get(id=class_id)
    class_ob = {
        'id': c.id,
        'name': c.name,
        'teachers': c.teachers.all(),
        'students_number': len(c.students.all()),
        'describe': c.describe,
        'cover': (c.cover + '?w=125&h=100'),
        'create_time': c.create_time,
    }
    data["class"] = class_ob
    # 拿出课程练习
    tests = []
    for t in c.test_set.all():
        test_ob = {
            "id": t.id,
            "name": t.name,
        }
        tests.append(test_ob)
    data["tests"] = tests
    statistics = {}
    for test in tests:
        statistics[test['id']] = []
    times = 1
    for one_record in stu_class_record:
        spend_time = 0
        spend_click = 0
        time_string_list = one_record.spend_time[1:-1].split(',')
        for time_string in time_string_list:
            spend_time = spend_time + int(time_string)
        click_string_list = one_record.spend_click[1:-1].split(',')
        for click_string in click_string_list:
            spend_click = spend_click + int(click_string)
        record_ob = {
            'times': times,
            'spend_time': spend_time,
            'spend_click': spend_click,
        }
        statistics[one_record.test_id].append(record_ob)
        times = times + 1
    print(statistics)
    data['statistics'] = statistics
    # 接下来在这里实现学生页面活跃榜数据获取
    class_records = models.DetailRecord.objects.filter(_class=class_id)
    students = c.students.all()
    hardworking_men = []
    for student in students:
        hardworking_men.append([student.username, 0])
    if class_records:
        for r in class_records:
            for man in hardworking_men:
                if r.user.username == man[0]:
                    man[1] += 1
    sorted_hardworking_men = sorted(hardworking_men, key=lambda x: x[1], reverse=True)
    Len = len(sorted_hardworking_men)
    if Len > 6:
        del (sorted_hardworking_men[5:Len - 1])
    if Len < 6:
        for i in range(Len, 6):
            sorted_hardworking_men.append(['虚位以待'])
    data['hardworking_men'] = sorted_hardworking_men
    # 活跃榜数据获取完成
    return render(request, 'grade/stu_one_class_grade.html', data)


def index(request):
    # 先判断用户身份
    user_id = request.session.get('user_id')
    user = User.objects.get(id=user_id)
    if user.identity == 'teacher':
        # 老师的话就显示他的课程，普遍成绩，点开之后还能看详细成绩
        classes = user.own_classes_set.all()
        return grade_classes_list(request)
        for cls in classes:
            print("!")
    else:
        # 学生的话就只显示他上过的课的成绩，点开之后查看详细成绩
        classes = user.join_classes_set.all()
        return stu_classes_list(request, user.id)


#   return render(request, 'grade/index.html')


def tea_class(request):
    return render(request, 'grade/tea_class.html')


# 拿出单个课程的数据
def one_class_grade(request, id):
    data = {}
    c = models.Class.objects.get(id=id)
    class_ob = {
        'id': c.id,
        'name': c.name,
        'teachers': c.teachers.all(),
        'students_number': len(c.students.all()),
        'describe': c.describe,
        'cover': (c.cover + '?w=125&h=100'),
        'create_time': c.create_time,
    }
    data["class"] = class_ob
    tests = []
    # 拿出有哪些练习
    for t in c.test_set.all():
        test_ob = {
            "id": t.id,
            "name": t.name,
        }
        tests.append(test_ob)
    data["tests"] = tests
    # 拿出学生名单
    class_students = c.students.all()

    students = []
    if class_students:
        for r in class_students:
            student_ob = {
                'id': r.id,
                'name': r.username,
            }
            students.append(student_ob)
    else:
        student_ob = {
            'id': '-1',
            'name': '暂无学生参与此课程'
        }
        students.append(student_ob)

    data["students"] = students

    # 初始化统计信息
    statistics = {}
    for t in tests:
        statistics[t['id']] = {}
        for s in students:
            t_statistics = {
                'do_times': 0,
                'average_time': 0.0,
                'average_click': 0.0,
            }
            statistics[t['id']][s['id']] = t_statistics
    # 初始化活跃榜
    hardworking_men = []
    for s in students:
        hardworking_men.append([s['name'], 0])
    # 拿出做练习记录的数据
    records = models.DetailRecord.objects.filter(_class=id).order_by("test_id", "user_id")
    if records:
        for r in records:
            amount_time = 0
            amount_click = 0
            time_string_list = r.spend_time[1:-1].split(',')
            for time_string in time_string_list:
                amount_time += int(time_string)
            click_string_list = r.spend_click[1:-1].split(',')
            for click_string in click_string_list:
                amount_click += int(click_string)
            statistics[r.test.id][r.user.id]['do_times'] += 1
            statistics[r.test.id][r.user.id]['average_time'] += amount_time
            statistics[r.test.id][r.user.id]['average_click'] += amount_click
            for man in hardworking_men:
                if r.user.username == man[0]:
                    man[1] += 1
        statistics[r.test.id][r.user.id]['average_time'] /= statistics[r.test.id][r.user.id]['do_times']
        statistics[r.test.id][r.user.id]['average_click'] /= statistics[r.test.id][r.user.id]['do_times']
    sorted_hardworking_men = sorted(hardworking_men, key=lambda x: x[1], reverse=True)
    Len = len(sorted_hardworking_men)
    if Len > 6:
        del(sorted_hardworking_men[5:Len-1])
    if Len < 6:
        for i in range(Len, 6):
            sorted_hardworking_men.append(['虚位以待'])
    data['statistics'] = statistics
    data['hardworking_men'] = sorted_hardworking_men
    # 到这数据处理完了

    user_id = request.session.get('user_id')
    user = User.objects.get(id=user_id)
    if user in c.students.all():
        data["is_member"] = True
    else:
        data["is_member"] = False
    if user in c.teachers.all():
        data["is_creator"] = True
    else:
        data["is_creator"] = False
    # 为了能够从练习中返回
    request.session["current_class"] = c.id
    return render(request, 'grade/one_class_grade.html', data)


# 拿出单个练习的数据
def one_test_grade(request, id):
    data = {}
    t = models.Test.objects.get(id=id)
    test_ob = {
        'id': t.id,
        'name': t.name,
        'create_time': t.create_time,
        'describe': t.describe,
        'empty_test': '',
        'teacher': t.teacher,
        'cover': (t.cover + '?w=250&h=200'),
    }
    if len(t.describe) == 0:
        test_ob['describe'] = "教师还没有为这个练习编写描述信息。"
    if t.data == '':
        test_ob['empty_test'] += "\n\n\n 注意 : 这是一个空练习，教师还没有编辑它的内容。"
    data['test'] = test_ob
    user_id = request.session.get('user_id')
    u = User.objects.get(id=user_id)
    if u == t.teacher:
        data['is_creator'] = True
    else:
        data['is_creator'] = False

    t1 = models.DetailRecord.objects.filter(test=id)
    print(t1)
    do_test = []
    if t1:
        for r in t1:
            test_ob1 = {
                'test_id': r.test,
                'user': r.user,
                'spend_time': r.spend_time,
                'spend_click': r.spend_click,
                'from_class': r._class,
            }
            do_test.append(test_ob1)
    else:
        do_test = ['该学生未完成该练习']
    print(do_test)
    data['do_test'] = do_test
    return render(request, 'grade/one_test_grade.html', data)
