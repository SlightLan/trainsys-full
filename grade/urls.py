from django.urls import path
from django.conf.urls import url
from test_op import test_op_views
from grade import grade_views

urlpatterns = [
    path('', grade_views.index),
    path('index/',grade_views.index),
    path('tea_class/',grade_views.tea_class),
    url(r'(\d+)/test', grade_views.one_test_grade, name='one_test_grade'),
    url(r'(\d+)/student/(\d+)',grade_views.stu_one_class_grade, name='stu_one_class_grade'),
    url(r'(\d+)/', grade_views.one_class_grade, name='one_class_grade'),
]